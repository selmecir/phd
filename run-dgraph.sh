#!/usr/bin/env bash
docker rm -f dgraph
docker run --rm -d -p 8080:8080 -p 9080:9080 --name dgraph dgraph/dgraph dgraphzero -w zw
docker exec -it dgraph dgraph --bindall=true --memory_mb 1024 -peer 127.0.0.1:8888