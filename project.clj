(defproject phd "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :main selmeci.run
  :aot :all
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/core.async "0.4.474"]
                 [org.clojure/core.match "0.3.0-alpha5"]
                 [org.clojure/core.memoize "0.5.9"]
                 [environ "1.1.0"]
                 [http-kit "2.2.0"]
                 [com.stuartsierra/component "0.3.2"]
                 [cheshire "5.8.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [camel-snake-kebab "0.4.0"]
                 [org.springframework.data/spring-data-neo4j "5.0.3.RELEASE"]

                 [edocu/neo4j-component "0.1.3-SNAPSHOT"]])
