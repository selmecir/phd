(ns selmeci.test.canvas
  (:require [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [selmeci.model.core :as model]
            [selmeci.canvas :as c]
            [selmeci.painter :as p]
            [selmeci.help.dgraph :as dgraph])
  (:import java.lang.AssertionError))

(deftest add-model!
  (testing "duplicate model in canvas"
    (let [dgraph (component/start
                   (dgraph/->dgraph))
          canvas (component/start
                   (c/map->Canvas {:dgraph dgraph}))
          painter (component/start
                    (p/->Painter canvas))
          model1 (model/->Model
                   :model1
                   (model/with-participants
                     service1 :Service
                     server1 :Server)
                   (model/with-relations
                     server1 :runs service1
                     service1 :isRunBy server1))]
      (p/draw! painter model1)
      (is (thrown-with-msg?
            AssertionError
            #"Model duplicates: :model1"
            (p/draw! painter model1))))))
