(ns selmeci.test.pattern-vocabulary.core
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as s]
            [selmeci.pattern-vocabulary.core :as core]))

(deftest valid-relation?
  (testing "with good definition"
    (let [entities-roles #{:role1 :role2}
          relations-roles #{:relation1 :relation2}]
      (is (true? (core/valid-relation?
                   entities-roles
                   relations-roles
                   [:role1 :relation1 :role2])))
      (is (true? (core/valid-relation?
                   entities-roles
                   relations-roles
                   [:role2 :relation2 :role1])))))
  (testing "with wrong definition"
    (let [entities-roles #{:roleA :roleB}
          relations-roles #{:relation1 :relation2}]
      (is (false? (core/valid-relation?
                    entities-roles
                    relations-roles
                    [:role1 :relation1 :role2])))
      (is (false? (core/valid-relation?
                    entities-roles
                    relations-roles
                    [:role2 :relation2 :role1]))))))

(deftest valid-pattern-vocabulary?
  (testing "with good definition"
    (let [entities-roles #{:role1 :role2}
          relations-roles #{:relation1 :relation2}
          entities-hierarchies #{}
          pattern-vocabulary {::core/entities-roles       entities-roles
                              ::core/relations-roles      relations-roles
                              ::core/entities-hierarchies entities-hierarchies
                              ::core/relations            #{[:role1 :relation1 :role2]
                                                       [:role2 :relation2 :role1]}}]
      (is (true? (s/valid? ::core/patter-vocabulary
                           pattern-vocabulary)))))
  (testing "with wrong definition"
    (let [entities-roles #{:roleA :roleB}
          relations-roles #{:relation1 :relation2}
          entities-hierarchies #{}
          pattern-vocabulary {::core/entities-roles       entities-roles
                              ::core/relations-roles      relations-roles
                              ::core/entities-hierarchies entities-hierarchies
                              ::core/relations            #{[:roleA :relation1 :roleB]
                                                       [:role2 :relation2 :role1]}}]
      (is (false? (s/valid? ::core/patter-vocabulary
                            pattern-vocabulary))))))
