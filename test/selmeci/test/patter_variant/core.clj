(ns selmeci.test.patter-variant.core
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as s]
            [selmeci.pattern-variant.core :as core]
            [selmeci.pattern-vocabulary.core :as pv]))

(deftest valid-pattern-variant
  (testing "with good definition"
    (let [entities-roles #{:role1 :role2}
          relations-roles #{:relation1 :relation2}
          entities-hierarchies #{}
          pattern-vocabulary {::pv/entities-roles       entities-roles
                              ::pv/relations-roles      relations-roles
                              ::pv/entities-hierarchies entities-hierarchies
                              ::pv/relations            #{[:role1 :relation1 :role2]
                                                     [:role2 :relation2 :role1]}}
          pattern-variant {::pv/patter-vocabulary pattern-vocabulary
                           ::core/participants    #{{::pv/role :role1
                                                     ::core/id :p1}
                                                    {::pv/role :role2
                                                     ::core/id :p2}}
                           ::core/relations       #{[:p1 :relation1 :p2]
                                                    [:p2 :relation2 :p1]}}]
      (s/explain ::core/pattern-variant
                 pattern-variant)
      (is (true? (s/valid? ::core/pattern-variant
                           pattern-variant)))))
  (testing "with wrong definition"
    (let [entities-roles #{:role1 :role2}
          relations-roles #{:relation1 :relation2}
          entities-hierarchies #{}
          pattern-vocabulary {::pv/entities-roles       entities-roles
                              ::pv/relations-roles      relations-roles
                              ::pv/entities-hierarchies entities-hierarchies
                              ::pv/relations            #{[:role1 :relation1 :role2]
                                                     [:role2 :relation2 :role1]}}
          pattern-variant {::pv/patter-vocabulary pattern-vocabulary
                           ::core/participants    #{{::pv/role :role1
                                                     ::core/id :p1}
                                                    {::pv/role :role1
                                                     ::core/id :p2}}
                           ::core/relations       #{[:p1 :relation1 :p2]
                                                    [:p2 :relation2 :p1]}}]
      (is (false? (s/valid? ::core/pattern-variant
                            pattern-variant))))))
