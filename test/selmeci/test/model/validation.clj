(ns selmeci.test.model.validation
  (:require [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [selmeci.model.core :as model]
            [selmeci.canvas :as c]
            [selmeci.painter :as p]))
