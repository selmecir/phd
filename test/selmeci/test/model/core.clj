(ns selmeci.test.model.core
  (:require [clojure.test :refer :all]
            [selmeci.model.core :as model]))

(deftest sync-models
  (let [model1 (model/->Model
                 :model1
                 (model/with-participants
                   work-report :Service
                   work-report-1 :ServiceInstance
                   server-alfa :Server)
                 (model/with-relations
                   work-report :isRunBy work-report-1
                   work-report-1 :runs work-report
                   server-alfa :deploys work-report-1
                   work-report-1 :isDeployedOn server-alfa))
        model2 (model/->Model
                 :model2
                 (model/with-participants
                   work-report :Service
                   work-report-2 :ServiceInstance
                   server-beta :Server)
                 (model/with-relations
                   work-report :isRunBy work-report-2
                   work-report-2 :runs work-report
                   server-beta :deploys work-report-2
                   work-report-2 :isDeployedOn server-beta))
        model3 (model/->Model
                 :model3
                 (model/with-participants
                   work-report :Database
                   work-report-1 :ServiceInstance
                   work-report-2 :ServiceInstance
                   server-alfa :Server)
                 (model/with-relations
                   work-report :isRunBy work-report-1
                   work-report-1 :runs work-report
                   server-alfa :deploys work-report-1
                   work-report-1 :isDeployedOn server-alfa
                   work-report :isRunBy work-report-2
                   work-report-2 :runs work-report
                   server-alfa :deploys work-report-2
                   work-report-2 :isDeployedOn server-alfa))]
    (is (= (model/sync-models
             :merge
             model1
             model2
             model3)
           (model/map->Model
             {:model-name :merge,
              :participants
                          #{{:selmeci.pattern-variant.core/id            :work-report-2,
                             :selmeci.pattern-vocabulary.vocabulary/role #{:ServiceInstance}}
                            {:selmeci.pattern-variant.core/id            :server-alfa,
                             :selmeci.pattern-vocabulary.vocabulary/role #{:Server}}
                            {:selmeci.pattern-variant.core/id            :work-report-1,
                             :selmeci.pattern-vocabulary.vocabulary/role #{:ServiceInstance}}
                            {:selmeci.pattern-variant.core/id            :server-beta,
                             :selmeci.pattern-vocabulary.vocabulary/role #{:Server}}
                            {:selmeci.pattern-variant.core/id            :work-report,
                             :selmeci.pattern-vocabulary.vocabulary/role #{:Service :Database}}},
              :relations
                          #{[:work-report-2 :isDeployedOn :server-beta]
                            [:work-report :isRunBy :work-report-1]
                            [:work-report-1 :isDeployedOn :server-alfa]
                            [:server-alfa :deploys :work-report-1]
                            [:server-beta :deploys :work-report-2]
                            [:server-alfa :deploys :work-report-2]
                            [:work-report-2 :isDeployedOn :server-alfa]
                            [:work-report-1 :runs :work-report]
                            [:work-report-2 :runs :work-report]
                            [:work-report :isRunBy :work-report-2]}})))))