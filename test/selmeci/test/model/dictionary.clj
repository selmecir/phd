(ns selmeci.test.model.dictionary
  (:require [clojure.test :refer :all]
            [com.stuartsierra.component :as component]
            [selmeci.model.dictionary :as dictionary]
            [selmeci.model.core :as model]
            [selmeci.canvas :as c]
            [selmeci.painter :as p]
            [selmeci.help.dgraph :as dgraph]))


(deftest roles-dic
  (let [dgraph (component/start
                 (dgraph/->dgraph))
        canvas (component/start
                 (c/map->Canvas {:dgraph dgraph}))
        painter (component/start
                  (p/->Painter canvas))
        dic (component/start
              (dictionary/map->ModelsDictionary
                {:canvas canvas}))
        model1 (model/->Model
                 :model1
                 (model/with-participants
                   service1 :Service
                   server1 :Server)
                 (model/with-relations
                   server1 :runs service1
                   service1 :isRunBy server1))
        model2 (model/->Model
                 :model2
                 (model/with-participants
                   service1 :Database
                   server1 :Server)
                 (model/with-relations
                   server1 :runs service1
                   service1 :isRunBy server1))]
    (p/draw! painter model1)
    (p/draw! painter model2)
    (is (= {:service1 #{:Service :Database}
            :server1  #{:Server}}
           (dictionary/roles-dic dic)))))

(deftest relations-dic
  (let [dgraph (component/start
                 (dgraph/->dgraph))
        canvas (component/start
                 (c/map->Canvas {:dgraph dgraph}))
        painter (component/start
                  (p/->Painter canvas))
        dic (component/start
              (dictionary/map->ModelsDictionary
                {:canvas canvas}))
        model1 (model/->Model
                 :model1
                 (model/with-participants
                   service1 :Service
                   server1 :Server)
                 (model/with-relations
                   server1 :runs service1
                   service1 :isRunBy server1))
        model2 (model/->Model
                 :model2
                 (model/with-participants
                   service1 :Database
                   server1 :Server)
                 (model/with-relations
                   server1 :runs service1
                   service1 :isRunBy server1))]
    (p/draw! painter model1)
    (p/draw! painter model2)
    (is (= {{:relation [:service1 :isRunBy :server1],
             :roles    [#{:Service :Database} :isRunBy #{:Server}]}
            #{:model1 :model2},
            {:relation [:server1 :runs :service1],
             :roles    [#{:Server} :runs #{:Service :Database}]}
            #{:model1 :model2}}
           (dictionary/relations-dic dic)))))