#!/usr/bin/env bash
docker pull neo4j
docker rm -f neo4j-phd
docker run --rm -it -p 7474:7474 -p 7687:7687 -e NEO4J_AUTH=none --name neo4j-phd neo4j