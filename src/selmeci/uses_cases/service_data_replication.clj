(ns selmeci.uses-cases.service-data-replication
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- problem-variant
  [pattern-vocabulary]
  (-> (model/->model
        pattern-vocabulary
        :service-data-replication-problem)
      (with-participants
        [:work-report :Service
         :work-report-1 :ServiceInstance
         :server-alfa :Server
         :mongodb :Service
         :mongodb-1 :ServiceInstance
         :work-reports-db :Database
         :statistics :Service
         :statistics-1 :ServiceInstance
         :server-beta :Server])
      (with-relations
        [:work-report :isRunBy :work-report-1
         :work-report-1 :runs :work-report
         :server-alfa :deploys :work-report-1
         :work-report-1 :isDeployedOn :server-alfa
         :mongodb-1 :isDeployedOn :server-alfa
         :server-alfa :deploys :mongodb-1
         :mongodb :isRunBy :mongodb-1
         :mongodb-1 :runs :mongodb
         :mongodb :provides :work-reports-db
         :work-reports-db :isProvidedBy :mongodb
         :work-reports-db :isStoredIn :server-alfa
         :server-alfa :stores :work-reports-db
         :work-report :worksWith :work-reports-db
         :work-reports-db :isUsedBy :work-report
         :statistics :worksWith :work-reports-db
         :work-reports-db :isUsedBy :statistics
         :statistics :isRunBy :statistics-1
         :statistics-1 :runs :statistics
         :statistics-1 :isDeployedOn :server-beta
         :server-beta :deploys :statistics-1])))

(defn- ok-variant
  [pattern-vocabulary]
  (-> (model/->model
        pattern-vocabulary
        :service-data-replication-ok)
      (with-participants
        [:elements :Service
         :elements-1 :ServiceInstance
         :server-gamma :Server
         :mongodb :Service
         :mongodb-1 :ServiceInstance
         :elements-comments-db :Database
         :views :Service
         :views-1 :ServiceInstance
         :server-delta :Server])
      (with-relations
        [:elements :isRunBy :elements-1
         :elements-1 :runs :elements
         :server-gamma :deploys :elements-1
         :elements-1 :isDeployedOn :server-gamma
         :mongodb-1 :isDeployedOn :server-gamma
         :server-gamma :deploys :mongodb-1
         :mongodb :isRunBy :mongodb-1
         :mongodb-1 :runs :mongodb
         :mongodb :provides :elements-comments-db
         :elements-comments-db :isProvidedBy :mongodb
         :elements-comments-db :isStoredIn :server-gamma
         :server-gamma :stores :elements-comments-db
         :elements-comments-db :isStoredIn :server-delta
         :server-delta :stores :elements-comments-db
         :elements :worksWith :elements-comments-db
         :elements-comments-db :isUsedBy :elements
         :views :worksWith :elements-comments-db
         :elements-comments-db :isUsedBy :views
         :views :isRunBy :views-1
         :views-1 :runs :views
         :views-1 :isDeployedOn :server-delta
         :server-delta :deploys :views-1])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Service data replication")
    (p/draw! painter (problem-variant pattern-vocabulary))
    (p/draw! painter (ok-variant pattern-vocabulary)
             #_(model/sync-models
                       pattern-vocabulary
                       :service-data-replication
                       (problem-variant pattern-vocabulary)
                       (ok-variant pattern-vocabulary)))
    (log/trace "UseCase: Service data replication. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
