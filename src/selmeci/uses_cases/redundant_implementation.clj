(ns selmeci.uses-cases.redundant-implementation
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- missing-redundant-instance
  [pattern-vocabulary]
  (-> (model/->model
        pattern-vocabulary
        :missing-redundant-instance)
      (with-participants
        [:service-ri1 :Service
         :service-instance-ri1 :ServiceInstance
         :service-ri2 :Service
         :service-ri3 :Service])
      (with-relations
        [:service-ri1 :communicatesWith :service-ri2
         :service-ri1 :communicatesWith :service-ri3
         :service-ri1 :isRunBy :service-instance-ri1
         :service-instance-ri1 :runs :service-ri1])))

(defn- redundant-implementation
  [pattern-vocabulary]
  (-> (model/->model
        pattern-vocabulary
        :redundant-implementation)
      (with-participants
        [:service-ri4 :Service
         :service-instance-ri2 :ServiceInstance
         :service-instance-ri3 :ServiceInstance
         :service-ri5 :Service
         :service-ri6 :Service])
      (with-relations
        [:service-ri4 :communicatesWith :service-ri5
         :service-ri4 :communicatesWith :service-ri6
         :service-ri4 :isRunBy :service-instance-ri2
         :service-instance-ri2 :runs :service-ri4
         :service-ri4 :isRunBy :service-instance-ri3
         :service-instance-ri3 :runs :service-ri4])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Redundant Implementation")
    (p/draw! painter (missing-redundant-instance pattern-vocabulary))
    (p/draw! painter (redundant-implementation pattern-vocabulary))
    (log/trace "UseCase: Redundant Implementation. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
