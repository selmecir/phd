(ns selmeci.uses-cases.process-abstraction
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- task-service
  [pattern-vocabulary]
  (log/trace "use case: task-service")
  (-> (model/->model
        pattern-vocabulary
        :task-service)
      (with-participants
        [:service-pa1 :Service
         :business-process-pa1 :BusinessProcess
         :layer-pa :TaskServiceLayer
         :functionality-pa1 :ProcessSpecificFunctionality
         :functionality-pa2 :ProcessSpecificFunctionality])
      (with-relations
        [:service-pa1 :isStoredIn :layer-pa
         :layer-pa :contains :service-pa1
         :service-pa1 :delivers :functionality-pa1
         :functionality-pa1 :isDeliveredBy :service-pa1
         :functionality-pa1 :belongsTo :business-process-pa1
         :business-process-pa1 :consistsOf :functionality-pa1
         :service-pa1 :delivers :functionality-pa2
         :functionality-pa2 :isDeliveredBy :service-pa1
         :functionality-pa2 :belongsTo :business-process-pa1
         :business-process-pa1 :consistsOf :functionality-pa2])))

(defn- task-service-is-not-in-layer
  [pattern-vocabulary]
  (log/trace "use case: task-service-is-not-in-layer")
  (-> (model/->model
        pattern-vocabulary
        :task-service-is-not-in-layer)
      (with-participants
        [:service-pa2 :Service
         :business-process-pa3 :BusinessProcess
         :functionality-pa5 :ProcessSpecificFunctionality
         :functionality-pa6 :ProcessSpecificFunctionality])
      (with-relations
        [:service-pa2 :delivers :functionality-pa5
         :functionality-pa5 :isDeliveredBy :service-pa2
         :functionality-pa5 :belongsTo :business-process-pa3
         :business-process-pa3 :consistsOf :functionality-pa5
         :service-pa2 :delivers :functionality-pa6
         :functionality-pa6 :isDeliveredBy :service-pa2
         :functionality-pa6 :belongsTo :business-process-pa3
         :business-process-pa3 :consistsOf :functionality-pa6])))

(defn- business-process-is-scattered
  [pattern-vocabulary]
  (log/trace "use case: business-process-is-scattered")
  (-> (model/->model
        pattern-vocabulary
        :business-process-is-scattered)
      (with-participants
        [:service-pa3 :Service
         :service-pa4 :Service
         :business-process-pa2 :BusinessProcess
         :functionality-pa3 :ProcessSpecificFunctionality
         :functionality-pa4 :ProcessSpecificFunctionality])
      (with-relations
        [:service-pa3 :delivers :functionality-pa3
         :functionality-pa3 :isDeliveredBy :service-pa3
         :functionality-pa3 :belongsTo :business-process-pa2
         :business-process-pa2 :consistsOf :functionality-pa3
         :service-pa4 :delivers :functionality-pa4
         :functionality-pa4 :isDeliveredBy :service-pa4
         :functionality-pa4 :belongsTo :business-process-pa2
         :business-process-pa2 :consistsOf :functionality-pa4])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Process Abstraction")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :process-abstraction-use-case
                       (business-process-is-scattered pattern-vocabulary)
                       (task-service pattern-vocabulary)
                       (task-service-is-not-in-layer pattern-vocabulary)))
    (log/trace "UseCase: Process Abstraction. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
