(ns selmeci.uses-cases.canonical-schema
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- data-model-transformation-is-need
  [pattern-vocabulary]
  (log/trace "use case: data-model-transformation-is-need")
  (-> (model/->model
        pattern-vocabulary
        :data-model-transformation-is-need)
      (with-participants
        [:service-a :Service
         :service-b :Service
         :data :Data
         :data-model-a :DataModel
         :data-model-b :DataModel])
      (with-relations
        [:service-a :communicatesWith :service-b
         :service-b :communicatesWith :service-a
         :service-a :produces :data
         :data :isProducedBy :service-a
         :service-b :consumes :data
         :data :isConsumedBy :service-b
         :service-a :uses :data-model-a
         :data-model-a :isUsedBy :service-a
         :service-b :uses :data-model-b
         :data-model-b :isUsedBy :service-b
         :data-model-a :definesStructureFor :data
         :data :isStructuredWith :data-model-a
         :data-model-b :definesStructureFor :data
         :data :isStructuredWith :data-model-b])))

(defn- data-model-transformation-exists
  [pattern-vocabulary]
  (log/trace "use case: data-model-transformation-exists")
  (-> (model/->model
        pattern-vocabulary
        :data-model-transformation-exists)
      (with-participants
        [:service-c :Service
         :service-d :Service
         :data :Data
         :data-model-c :DataModel
         :data-model-d :DataModel
         :transformation :DataModelTransformation])
      (with-relations
        [:service-c :communicatesWith :service-d
         :service-d :communicatesWith :service-c
         :service-c :produces :data
         :data :isProducedBy :service-c
         :service-d :consumes :data
         :data :isConsumedBy :service-d
         :service-c :uses :data-model-c
         :data-model-c :isUsedBy :service-c
         :service-d :uses :data-model-d
         :data-model-d :isUsedBy :service-d
         :data-model-c :definesStructureFor :data
         :data :isStructuredWith :data-model-c
         :data-model-d :definesStructureFor :data
         :data :isStructuredWith :data-model-d
         :transformation :transformsFrom :data-model-c
         :data-model-c :isTransformedBy :transformation
         :transformation :transformsTo :data-model-d
         :data-model-d :isTransformedBy :transformation])))

(defn- canonical-schema-is-used
  [pattern-vocabulary]
  (log/trace "use case: canonical-schema-is-used")
  (-> (model/->model
        pattern-vocabulary
        :canonical-schema-is-used)
      (with-participants
        [:service-e :Service
         :service-f :Service
         :data :Data
         :data-model-e :DataModel])
      (with-relations
        [:service-e :communicatesWith :service-f
         :service-f :communicatesWith :service-e
         :service-e :produces :data
         :data :isProducedBy :service-e
         :service-f :consumes :data
         :data :isConsumedBy :service-f
         :service-e :uses :data-model-e
         :data-model-e :isUsedBy :service-e
         :service-f :uses :data-model-e
         :data-model-e :isUsedBy :service-f
         :data-model-e :definesStructureFor :data
         :data :isStructuredWith :data-model-e])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Canonical Schema")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :canonical-schema-use-case
                       (data-model-transformation-is-need pattern-vocabulary)
                       (data-model-transformation-exists pattern-vocabulary)
                       (canonical-schema-is-used pattern-vocabulary)))
    (log/trace "UseCase: Canonical Schema. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))