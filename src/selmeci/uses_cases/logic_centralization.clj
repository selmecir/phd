(ns selmeci.uses-cases.logic-centralization
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- function-logic-is-implemented-in-different-services
  [pattern-vocabulary]
  (log/trace "use case: function-logic-is-implemented-in-different-services")
  (-> (model/->model
        pattern-vocabulary
        :function-logic-is-implemented-in-different-services)
      (with-participants
        [:service-lc1 :Service
         :service-lc2 :Service
         :invoice-logic :FunctionLogic
         :functionality-lc1 :Functionality
         :functionality-lc2 :Functionality])
      (with-relations
        [:service-lc1 :delivers :functionality-lc1
         :functionality-lc1 :isDeliveredBy :service-lc1
         :functionality-lc1 :belongsTo :invoice-logic
         :invoice-logic :consistsOf :functionality-lc1
         :service-lc2 :delivers :functionality-lc1
         :functionality-lc2 :isDeliveredBy :service-lc2
         :functionality-lc2 :belongsTo :invoice-logic
         :invoice-logic :consistsOf :functionality-lc2])))

(defn- logic-centralization-candidate
  [pattern-vocabulary]
  (log/trace "use case: logic-centralization-candidate")
  (-> (model/->model
        pattern-vocabulary
        :logic-centralization-candidate)
      (with-participants
        [:service-lc3 :Service
         :function-logic1 :FunctionLogic
         :functionality-lc3 :Functionality
         :functionality-lc4 :Functionality])
      (with-relations
        [:service-lc3 :delivers :functionality-lc3
         :functionality-lc3 :isDeliveredBy :service-lc3
         :functionality-lc3 :belongsTo :function-logic1
         :function-logic1 :consistsOf :functionality-lc3
         :service-lc3 :delivers :functionality-lc3
         :functionality-lc4 :isDeliveredBy :service-lc3
         :functionality-lc4 :belongsTo :function-logic1
         :function-logic1 :consistsOf :functionality-lc4])))

(defn- logic-centralization
  [pattern-vocabulary]
  (log/trace "use case: logic-centralization")
  (-> (model/->model
        pattern-vocabulary
        :logic-centralization)
      (with-participants
        [:service-lc4 :Service
         :function-logic2 :FunctionLogic
         :functionality-lc5 :Functionality
         :functionality-lc6 :Functionality])
      (with-relations
        [:service-lc4 :isOfficialFor :function-logic2
         :function-logic2 :restricts :service-lc4
         :service-lc4 :delivers :functionality-lc5
         :functionality-lc5 :isDeliveredBy :service-lc4
         :functionality-lc5 :belongsTo :function-logic2
         :function-logic2 :consistsOf :functionality-lc5
         :service-lc4 :delivers :functionality-lc5
         :functionality-lc6 :isDeliveredBy :service-lc4
         :functionality-lc6 :belongsTo :function-logic2
         :function-logic2 :consistsOf :functionality-lc6])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Logic Centralization")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :logic-centralization-use-case
                       (function-logic-is-implemented-in-different-services pattern-vocabulary)
                       (logic-centralization-candidate pattern-vocabulary)
                       (logic-centralization pattern-vocabulary)))
    (log/trace "UseCase: Logic Centralization. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
