(ns selmeci.uses-cases.entity-abstraction
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- service-delivers-functionality-from-different-business-entities
  [pattern-vocabulary]
  (log/trace "use case: service-delivers-non-business-functionality")
  (-> (model/->model
        pattern-vocabulary
        :service-delivers-functionality-from-different-business-entities)
      (with-participants
        [:service-ea1 :Service
         :business-entity-ea1 :BusinessEntity
         :business-entity-ea2 :BusinessEntity
         :functionality-ea1 :ProcessAgnosticFunctionality
         :functionality-ea2 :ProcessAgnosticFunctionality])
      (with-relations
        [:service-ea1 :delivers :functionality-ea1
         :functionality-ea1 :isDeliveredBy :service-ea1
         :functionality-ea1 :belongsTo :business-entity-ea1
         :business-entity-ea1 :defines :functionality-ea1
         :service-ea1 :delivers :functionality-ea2
         :functionality-ea2 :isDeliveredBy :service-ea1
         :functionality-ea2 :belongsTo :business-entity-ea2
         :business-entity-ea2 :defines :functionality-ea2])))

(defn- entity-service-is-not-entity-layer
  [pattern-vocabulary]
  (log/trace "use case: entity-service-is-not-entity-layer")
  (-> (model/->model
        pattern-vocabulary
        :entity-service-is-not-entity-layer)
      (with-participants
        [:service-ea2 :Service
         :business-entity-ea1 :BusinessEntity
         :functionality-ea3 :ProcessAgnosticFunctionality
         :functionality-ea4 :ProcessAgnosticFunctionality])
      (with-relations
        [:service-ea2 :delivers :functionality-ea3
         :functionality-ea3 :isDeliveredBy :service-ea2
         :functionality-ea3 :belongsTo :business-entity-ea1
         :business-entity-ea1 :defines :functionality-ea3
         :service-ea2 :delivers :functionality-ea4
         :functionality-ea4 :isDeliveredBy :service-ea2
         :functionality-ea4 :belongsTo :business-entity-ea1
         :business-entity-ea1 :defines :functionality-ea4])))

(defn- entity-service
  [pattern-vocabulary]
  (log/trace "use case: entity-service")
  (-> (model/->model
        pattern-vocabulary
        :entity-service)
      (with-participants
        [:service-ea3 :Service
         :business-entity-ea1 :BusinessEntity
         :layer-ea :EntityServiceLayer
         :functionality-ea5 :ProcessAgnosticFunctionality
         :functionality-ea6 :ProcessAgnosticFunctionality])
      (with-relations
        [:service-ea3 :isStoredIn :layer-ea
         :layer-ea :contains :service-ea3
         :service-ea3 :delivers :functionality-ea5
         :functionality-ea5 :isDeliveredBy :service-ea3
         :functionality-ea5 :belongsTo :business-entity-ea1
         :business-entity-ea1 :defines :functionality-ea5
         :service-ea3 :delivers :functionality-ea6
         :functionality-ea6 :isDeliveredBy :service-ea3
         :functionality-ea6 :belongsTo :business-entity-ea1
         :business-entity-ea1 :defines :functionality-ea6])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Entity Abstraction")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :entity-abstraction-use-case
                       (entity-service pattern-vocabulary)
                       (entity-service-is-not-entity-layer pattern-vocabulary)
                       (service-delivers-functionality-from-different-business-entities pattern-vocabulary)))
    (log/trace "UseCase: Entity Abstraction. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
