(ns selmeci.uses-cases.service-layers
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- service-layers-are-not-defined-in-inventory
  [pattern-vocabulary]
  (log/trace "use case: service-layers-are-not-defined-in-inventory")
  (-> (model/->model
        pattern-vocabulary
        :service-layers-are-not-defined-in-inventory)
      (with-participants
        [:inventory-sl1 :Inventory
         :service-sl1 :Service])
      (with-relations
        [:inventory-sl1 :contains :service-sl1
         :service-sl1 :isStoredIn :inventory-sl1])))

(defn- services-in-layer-use-different-service-model
  [pattern-vocabulary]
  (log/trace "use case: services-in-layer-use-different-service-model")
  (-> (model/->model
        pattern-vocabulary
        :services-in-layer-use-different-service-model)
      (with-participants
        [:service-layer-sl1 :ServiceLayer
         :service-sl2 :Service
         :service-sl3 :Service
         :service-model-sl1 :ServiceModel
         :service-model-sl2 :ServiceModel])
      (with-relations
        [:service-layer-sl1 :contains :service-sl2
         :service-sl2 :isStoredIn :service-layer-sl1
         :service-sl2 :isModeledWith :service-model-sl1
         :service-model-sl1 :models :service-sl2
         :service-layer-sl1 :contains :service-sl3
         :service-sl3 :isStoredIn :service-layer-sl1
         :service-sl3 :isModeledWith :service-model-sl2
         :service-model-sl2 :models :service-sl3])))

(defn- inventory-contains-service-without-service-layer
  [pattern-vocabulary]
  (log/trace "use case: inventory-contains-service-without-service-layer")
  (-> (model/->model
        pattern-vocabulary
        :inventory-contains-service-without-service-layer)
      (with-participants
        [:inventory-sl2 :Inventory
         :service-layer-sl2 :ServiceLayer
         :service-sl4 :Service])
      (with-relations
        [:inventory-sl2 :contains :service-sl4
         :service-sl4 :isStoredIn :inventory-sl2
         :inventory-sl2 :contains :service-layer-sl2
         :service-layer-sl2 :isStoredIn :inventory-sl2])))

(defn- service-layers
  [pattern-vocabulary]
  (log/trace "use case: inventory-contains-service-without-service-layer")
  (-> (model/->model
        pattern-vocabulary
        :inventory-contains-service-without-service-layer)
      (with-participants
        [:inventory-sl3 :Inventory
         :service-layer-sl3 :ServiceLayer
         :service-layer-sl4 :ServiceLayer
         :service-sl5 :Service
         :service-sl6 :Service])
      (with-relations
        [:inventory-sl3 :contains :service-sl5
         :service-sl5 :isStoredIn :inventory-sl3
         :inventory-sl3 :contains :service-sl6
         :service-sl6 :isStoredIn :inventory-sl3
         :inventory-sl3 :contains :service-layer-sl3
         :service-layer-sl3 :isStoredIn :inventory-sl3
         :inventory-sl3 :contains :service-layer-sl4
         :service-layer-sl4 :isStoredIn :inventory-sl3
         :service-layer-sl3 :contains :service-sl5
         :service-sl5 :isStoredIn :service-layer-sl3
         :service-layer-sl4 :contains :service-sl6
         :service-sl6 :isStoredIn :service-layer-sl4])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Service Layers")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :service-layers-use-case
                       (service-layers-are-not-defined-in-inventory pattern-vocabulary)
                       (services-in-layer-use-different-service-model pattern-vocabulary)
                       (inventory-contains-service-without-service-layer pattern-vocabulary)
                       (service-layers pattern-vocabulary)))
    (log/trace "UseCase: Service Layers. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
