(ns selmeci.uses-cases.canonical-protocol
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- missing-standardized-communication-protocol
  [pattern-vocabulary]
  (log/trace "use case: missing-standardized-communication-protocol")
  (-> (model/->model
        pattern-vocabulary
        :missing-standard-communication-protocol)
      (with-participants
        [:inventory-a :Inventory
         :inventory-b :Inventory
         :http :CommunicationProtocol])
      (with-relations
        [:inventory-a :standardizes :http
         :http :isStandardizedIn :inventory-a])))

(defn- services-in-inventory-do-not-use-standardized-protocol
  [pattern-vocabulary]
  (log/trace "use case: services-in-inventory-do-not-use-standardized-protocol")
  (-> (model/->model
        pattern-vocabulary
        :services-in-inventory-do-not-use-standardized-protocol)
      (with-participants
        [:inventory-c :Inventory
         :service-a :Service
         :service-b :Service
         :http :CommunicationProtocol])
      (with-relations
        [:inventory-c :standardizes :http
         :http :isStandardizedIn :inventory-c
         :service-a :isStoredIn :inventory-c
         :inventory-c :contains :service-a
         :service-b :isStoredIn :inventory-c
         :inventory-c :contains :service-b
         :service-a :uses :http
         :http :isUsedBy :service-a])))

(defn- services-in-inventory-use-standardized-protocol
  [pattern-vocabulary]
  (log/trace "use case: services-in-inventory-use-standardized-protocol")
  (-> (model/->model
        pattern-vocabulary
        :services-in-inventory-use-standardized-protocol)
      (with-participants
        [:inventory-d :Inventory
         :service-c :Service
         :service-d :Service
         :http :CommunicationProtocol])
      (with-relations
        [:inventory-d :standardizes :http
         :http :isStandardizedIn :inventory-d
         :service-c :isStoredIn :inventory-d
         :inventory-d :contains :service-c
         :service-d :isStoredIn :inventory-d
         :inventory-d :contains :service-d
         :service-c :uses :http
         :http :isUsedBy :service-c
         :service-d :uses :http
         :http :isUsedBy :service-d])))

(defn- all-services-in-inventory-use-other-protocol
  [pattern-vocabulary]
  (log/trace "use case: all-services-in-inventory-use-other-protocol")
  (-> (model/->model
        pattern-vocabulary
        :all-services-in-inventory-use-other-protocol)
      (with-participants
        [:inventory-e :Inventory
         :service-e :Service
         :service-f :Service
         :http :CommunicationProtocol
         :jms :CommunicationProtocol])
      (with-relations
        [:inventory-e :standardizes :http
         :http :isStandardizedIn :inventory-e
         :service-e :isStoredIn :inventory-e
         :inventory-e :contains :service-e
         :service-f :isStoredIn :inventory-e
         :inventory-e :contains :service-f
         :service-e :uses :jms
         :jms :isUsedBy :service-e
         :service-f :uses :jms
         :jms :isUsedBy :service-f])))

(defn- all-services-in-inventory-use-one-protocol-and-inventory-missing-standardized-protocol
  [pattern-vocabulary]
  (log/trace "use case: all-services-in-inventory-use-one-protocol-and-inventory-missing-standardized-protocol")
  (-> (model/->model
        pattern-vocabulary
        :all-services-in-inventory-use-other-protocol)
      (with-participants
        [:inventory-f :Inventory
         :inventory-g :Inventory
         :service-g :Service
         :service-h :Service
         :service-i :Service
         :service-j :Service
         :http :CommunicationProtocol
         :jms :CommunicationProtocol])
      (with-relations
        [:service-g :isStoredIn :inventory-f
         :inventory-f :contains :service-g
         :service-h :isStoredIn :inventory-f
         :inventory-f :contains :service-h
         :service-g :uses :jms
         :jms :isUsedBy :service-g
         :service-h :uses :jms
         :jms :isUsedBy :service-h

         :service-i :isStoredIn :inventory-g
         :inventory-g :contains :service-i
         :service-j :isStoredIn :inventory-g
         :inventory-g :contains :service-j
         :service-i :uses :jms
         :jms :isUsedBy :service-i
         :service-j :uses :http
         :http :isUsedBy :service-j])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Canonical Protocol")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :canonical-protocol-use-case
                       (missing-standardized-communication-protocol pattern-vocabulary)
                       (services-in-inventory-do-not-use-standardized-protocol pattern-vocabulary)
                       (services-in-inventory-use-standardized-protocol pattern-vocabulary)
                       (all-services-in-inventory-use-other-protocol pattern-vocabulary)
                       (all-services-in-inventory-use-one-protocol-and-inventory-missing-standardized-protocol pattern-vocabulary)))
    (log/trace "UseCase: Canonical Protocol. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
