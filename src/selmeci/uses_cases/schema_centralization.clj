(ns selmeci.uses-cases.schema-centralization
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- data-model-in-several-schemas
  [pattern-vocabulary]
  (log/trace "use case: data-model-in-several-schemas")
  (-> (model/->model
        pattern-vocabulary
        :data-model-in-several-schemas)
      (with-participants
        [:data-model-sc1 :DataModel
         :schema-sc1 :DataSchema
         :schema-sc2 :DataSchema])
      (with-relations
        [:schema-sc1 :describes :data-model-sc1
         :data-model-sc1 :isDescribedBy :schema-sc1
         :schema-sc2 :describes :data-model-sc1
         :data-model-sc1 :isDescribedBy :schema-sc2])))

(defn- schema-centralization
  [pattern-vocabulary]
  (log/trace "use case: schema-centralization")
  (-> (model/->model
        pattern-vocabulary
        :schema-centralization)
      (with-participants
        [:data-model-sc2 :DataModel
         :schema-sc3 :DataSchema])
      (with-relations
        [:schema-sc3 :describes :data-model-sc2
         :data-model-sc2 :isDescribedBy :schema-sc3])))


(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Process Abstraction")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :schema-centralization-use-case
                       (data-model-in-several-schemas pattern-vocabulary)
                       (schema-centralization pattern-vocabulary)))
    (log/trace "UseCase: Process Abstraction. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
