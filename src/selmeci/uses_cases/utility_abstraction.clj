(ns selmeci.uses-cases.utility-abstraction
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- service-delivers-non-business-functionalists
  [pattern-vocabulary]
  (log/trace "use case: service-delivers-non-business-functionality")
  (-> (model/->model
        pattern-vocabulary
        :service-delivers-non-business-functionality)
      (with-participants
        [:service-ua1 :Service
         :functionality-a :NonBusinessFunctionality
         :functionality-b :ProcessAgnosticFunctionality])
      (with-relations
        [:service-ua1 :delivers :functionality-a
         :functionality-a :isDeliveredBy :service-ua1
         :service-ua1 :delivers :functionality-b
         :functionality-b :isDeliveredBy :service-ua1])))

(defn- service-delivers-only-non-business-functionalists
  [pattern-vocabulary]
  (log/trace "use case: service-delivers-non-business-functionality")
  (-> (model/->model
        pattern-vocabulary
        :service-delivers-only-non-business-functionalists)
      (with-participants
        [:service-ua2 :Service
         :functionality-a :NonBusinessFunctionality
         :functionality-c :NonBusinessFunctionality])
      (with-relations
        [:service-ua2 :delivers :functionality-a
         :functionality-a :isDeliveredBy :service-ua2
         :service-ua2 :delivers :functionality-c
         :functionality-c :isDeliveredBy :service-ua2])))

(defn- utility-service-example
  [pattern-vocabulary]
  (log/trace "use case: utility-service-example")
  (-> (model/->model
        pattern-vocabulary
        :utility-service-example)
      (with-participants
        [:service-ua3 :UtilityService
         :functionality-ua1 :NonBusinessFunctionality
         :functionality-ua2 :NonBusinessFunctionality
         :utility-layer :UtilityServiceLayer])
      (with-relations
        [:service-ua3 :delivers :functionality-ua1
         :functionality-ua1 :isDeliveredBy :service-ua3
         :service-ua3 :delivers :functionality-ua2
         :functionality-ua2 :isDeliveredBy :service-ua3
         :service-ua3 :isStoredIn :utility-layer
         :utility-layer :contains :service-ua3])))

(defn- utility-service-deliver-non-business-functionalists
  [pattern-vocabulary]
  (log/trace "use case: utility-service-deliver-non-business-functionalists")
  (-> (model/->model
        pattern-vocabulary
        :utility-service-deliver-non-business-functionalists)
      (with-participants
        [:service-ua4 :UtilityService
         :functionality-ua1 :NonBusinessFunctionality
         :functionality-b :ProcessAgnosticFunctionality
         :utility-layer :UtilityServiceLayer])
      (with-relations
        [:service-ua4 :delivers :functionality-ua1
         :functionality-ua1 :isDeliveredBy :service-ua4
         :service-ua4 :delivers :functionality-b
         :functionality-b :isDeliveredBy :service-ua4
         :service-ua4 :isStoredIn :utility-layer
         :utility-layer :contains :service-ua4])))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Utility Abstraction")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :utility-abstraction-use-case
                       (service-delivers-non-business-functionalists pattern-vocabulary)
                       (service-delivers-only-non-business-functionalists pattern-vocabulary)
                       (utility-service-example pattern-vocabulary)
                       (utility-service-deliver-non-business-functionalists pattern-vocabulary)))
    (log/trace "UseCase: Utility Abstraction. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))
