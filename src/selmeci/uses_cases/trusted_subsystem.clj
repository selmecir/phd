(ns selmeci.uses-cases.trusted-subsystem
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defn- service-resource-is-accessed-by-consumer
  [pattern-vocabulary]
  (log/trace "use case: service-resource-is-accessed-by-consumer")
  (-> (model/->model
        pattern-vocabulary
        :service-resource-is-accessed-by-consumer)
      (with-participants
        [:consumer-ts1 :Consumer
         :database-ts1 :ServiceResource])
      (with-relations
        [:consumer-ts1 :access :database-ts1
         :database-ts1 :isAccessedBy :consumer-ts1])))

(defn- service-resource-is-in-trusted-subsystem
  [pattern-vocabulary]
  (log/trace "use case: service-resource-is-in-trusted-subsystem")
  (-> (model/->model
        pattern-vocabulary
        :service-resource-is-in-trusted-subsystem)
      (with-participants
        [:consumer-ts2 :Consumer
         :service-ts1 :Service
         :credentials-ts1 :ResourceCredentials
         :trust-boundary-ts1 :TrustBoundary
         :database-ts2 :ServiceResource])
      (with-relations
        [:consumer-ts2 :communicatesWith :service-ts1
         :service-ts1 :communicatesWith :consumer-ts2
         :service-ts1 :access :database-ts2
         :database-ts2 :isAccessedBy :service-ts1
         :service-ts1 :owns :credentials-ts1
         :credentials-ts1 :isOwnedBy :service-ts1
         :credentials-ts1 :secures :database-ts2
         :database-ts2 :isSecuredBy :credentials-ts1
         :service-ts1 :belongsTo :trust-boundary-ts1
         :trust-boundary-ts1 :contains :service-ts1
         :database-ts2 :belongsTo :trust-boundary-ts1
         :trust-boundary-ts1 :contains :database-ts2])))

(defn- service-access-resource-without-credentials
  [pattern-vocabulary]
  (log/trace "use case: service-access-resource-without-credentials")
  (-> (model/->model
        pattern-vocabulary
        :service-resource-is-in-trusted-subsystem)
      (with-participants
        [:consumer-ts3 :Consumer
         :service-ts2 :Service
         :trust-boundary-ts2 :TrustBoundary
         :database-ts3 :ServiceResource
         :credentials-ts2 :ResourceCredentials])
      (with-relations
        [:consumer-ts3 :communicatesWith :service-ts2
         :service-ts2 :communicatesWith :consumer-ts3
         :service-ts2 :access :database-ts3
         :database-ts3 :isAccessedBy :service-ts2
         :credentials-ts2 :secures :database-ts3
         :database-ts3 :isSecuredBy :credentials-ts2
         :service-ts2 :belongsTo :trust-boundary-ts2
         :trust-boundary-ts2 :contains :service-ts2
         :database-ts3 :belongsTo :trust-boundary-ts2
         :trust-boundary-ts2 :contains :database-ts3])))


(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Trusted Subsystem")
    (p/draw! painter (model/sync-models
                       pattern-vocabulary
                       :trusted-subsystem-use-case
                       (service-resource-is-accessed-by-consumer pattern-vocabulary)
                       (service-access-resource-without-credentials pattern-vocabulary)
                       (service-resource-is-in-trusted-subsystem pattern-vocabulary)))
    (log/trace "UseCase: Trusted Subsystem. drown")
    this)
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))