(ns selmeci.uses-cases.invalid-relation
  (:require [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.painter :as p]
            [selmeci.model.core :as model :refer [with-participants with-relations]]))

(defrecord UseCase [pattern-vocabulary painter]
  component/Lifecycle
  (start [this]
    (log/trace "UseCase: Invalid Relation in model")
    (let [model1 (-> (model/->model
                       pattern-vocabulary
                       :model1)
                     (with-participants
                       [:service1 :Service
                        :server1 :Server])
                     (with-relations
                       [:server1 :runs :service1
                        :service1 :isRunBy :server1]))]
      (p/draw! painter model1)))
  (stop [this]
    this))

(defn ->use-case
  "Return component which use painter to initialize use case data"
  []
  (map->UseCase {}))