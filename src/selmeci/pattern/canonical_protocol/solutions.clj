(ns selmeci.pattern.canonical-protocol.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->services-in-inventory-use-standardized-protocol
  []
  (pv/->pattern-variant
    :CanonicalProtocol
    :services-in-inventory-use-standardized-protocol
    "MATCH (inv:Inventory)-[:contains]->(s:Service)
     WITH inv, collect(DISTINCT s) as allServices
     MATCH (inv)-[:standardizes]->(std:CommunicationProtocol),
           (inv)-[:contains]->(s:Service)-[:uses]->(std)
     WITH inv, allServices,
	        collect(DISTINCT s) as stdServices
     WHERE allServices = stdServices
     RETURN inv"))
