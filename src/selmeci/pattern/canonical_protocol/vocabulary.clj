(ns selmeci.pattern.canonical-protocol.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :CanonicalProtocol
    #{:Inventory :Service :CommunicationProtocol}
    #{}
    #{:contains :isStoredIn
      :uses :isUsedBy
      :standardizes :isStandardizedIn}
    #{[:Inventory :contains :Service]
      [:Service :isStoredIn :Inventory]
      [:Service :uses :CommunicationProtocol]
      [:CommunicationProtocol :isUsedBy :Service]
      [:Inventory :standardizes :CommunicationProtocol]
      [:CommunicationProtocol :isStandardizedIn :Inventory]}))