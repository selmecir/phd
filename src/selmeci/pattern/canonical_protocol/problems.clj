(ns selmeci.pattern.canonical-protocol.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->missing-standardized-communication-protocol
  []
  (pv/->pattern-variant
    :CanonicalProtocol
    :missing-standardized-communication-protocol
    "MATCH (inv:Inventory)
     WHERE NOT (inv)-[:standardizes]->(:CommunicationProtocol)
     RETURN inv as inventory"))

(defn ->services-in-inventory-do-not-use-standardized-protocol
  []
  (pv/->pattern-variant
    :CanonicalProtocol
    :services-in-inventory-do-not-use-standardized-protocol
    "MATCH (inv:Inventory)-[:standardizes]->(stdProtocol:CommunicationProtocol),
           (inv)-[:contains]->(service:Service)
     WHERE NOT (service)-[:uses]->(stdProtocol)
     RETURN inv as inventory, service as service"))

(defn ->all-services-in-inventory-use-other-protocol
  []
  (pv/->pattern-variant
    :CanonicalProtocol
    :all-services-in-inventory-use-other-protocol
    "MATCH (inv:Inventory)-[:contains]->(s:Service)
     WITH inv, collect(DISTINCT s) as allServices
     MATCH (inv)-[:standardizes]->(std:CommunicationProtocol),
	         (inv)-[:contains]->(s:Service)
     WHERE NOT (s)-[:uses]->(std)
     WITH inv, allServices,
	        collect(DISTINCT s) as notStdServices
     WHERE allServices = notStdServices
     RETURN inv as inventory"))

(defn ->all-services-in-inventory-use-one-protocol-and-inventory-missing-standardized-protocol
  []
  (pv/->pattern-variant
    :CanonicalProtocol
    :all-services-in-inventory-use-one-protocol-and-inventory-missing-standardized-protocol
    "MATCH (inv:Inventory)-[:contains]->(s:Service)
     WHERE NOT (inv)-[:standardizes]->(:CommunicationProtocol)
     WITH inv, collect(DISTINCT s) as allServices
     MATCH (inv)-[:contains]->(s:Service)-[:uses]->(p:CommunicationProtocol)
     WITH inv, allServices,
	        collect(DISTINCT s) as stdServices,
          count(DISTINCT p) as protocols
     WHERE allServices = stdServices AND
	         protocols = 1
     RETURN inv"))

;;; dgraph

#_(defn ->all-services-in-inventory-use-other-protocol
    []
    (pv/->pattern-variant
      :CanonicalProtocol
      :all-services-in-inventory-use-other-protocol
      "{
          PP as var(func: eq(name, \"PatternParticipant\")){}
          SERVICE as var(func: eq(name, \"Service\")) @cascade{
            isA @filter(uid(PP))
          }
          COMM_PROT as var(func: eq(name, \"CommunicationProtocol\")) @cascade{
            isA @filter(uid(PP))
          }
          INV_ROLE as var(func: eq(name, \"Inventory\")) @cascade {
            isA @filter(uid(PP))
          }

          var(func: uid(INV_ROLE)) @cascade {
            INV as ~isA {
              contains {
                I as math(1)
                isA @filter(uid(SERVICE))
              }
              ALL_SERVICES as sum(val(I))
              STD_COMM_PROT as standardizes {
                isA @filter(uid(COMM_PROT))
              }
            }
          }

          var(func: uid(INV)) @cascade {
            contains {
              isA @filter(uid(SERVICE))
              J as math(1)
              uses @filter(not uid(STD_COMM_PROT)){
                isA @filter(uid(COMM_PROT))
              }
            }
            NS_SVRS as sum(val(J))
          }

          var(func: uid(INV)) {
            N as math(ALL_SERVICES - NS_SVRS)
          }

          expansions(func: uid(INV)) @filter(eq(val(N),0))  @cascade{
            name
            id: _uid_
            standardizes {
              name
              id: _uid_
              isA @filter(uid(COMM_PROT))
            }
            contains {
              name
              isA @filter(uid(SERVICE))
              uses {
                isA @filter(uid(COMM_PROT))
                name
                id: _uid_
              }
            }
          }
       }"))

#_(defn ->services-in-inventory-do-not-use-standardized-protocol
    []
    (pv/->pattern-variant
      :CanonicalProtocol
      :services-in-inventory-do-not-use-standardized-protocol
      "{
          PP as var(func: eq(name, \"PatternParticipant\")){}
          SERVICE as var(func: eq(name, \"Service\")) @cascade{
            isA @filter(uid(PP))
          }
          COMM_PROT as var(func: eq(name, \"CommunicationProtocol\")) @cascade{
            isA @filter(uid(PP))
          }

          var(func: eq(name, \"Inventory\")) @cascade {
            isA @filter(uid(PP))
            INV as ~isA {
              COMM_PROT as standardizes @filter(uid(COMM_PROT)){}
            }
          }

          #standardized services
          var(func: uid(INV)) @cascade{
            STD as contains{
              isA @filter(uid(SERVICE))
              uses @filter(uid(COMM_PROT))
            }
          }

          expansions(func: uid(INV)) @cascade {
            name
            id: _uid_
            standardizes @filter(uid(COMM_PROT)){
              name
              id: _uid_
            }
            problems: contains @filter(not uid(STD)){
              isA @filter(uid(SERVICE))
              name
              id: _uid_
            }
          }
       }"))

#_(defn ->missing-standardized-communication-protocol
    []
    (pv/->pattern-variant
      :CanonicalProtocol
      :missing-standardized-communication-protocol
      "{
          PP as var(func: eq(name, \"PatternParticipant\")){}
          var(func: eq(name, \"Inventory\")) @cascade {
            isA @filter(uid(PP))
            INV as ~isA
          }

          PROBLEMS as var(func: uid(INV)) @filter(eq(count(standardizes),0)){}

          expansions(func: uid(PROBLEMS)) @cascade {
            name
            id: _uid_
          }
       }"))

#_(defn ->all-services-in-inventory-use-one-protocol-and-inventory-missing-standardized-protocol
    []
    (pv/->pattern-variant
      :CanonicalProtocol
      :all-services-in-inventory-use-one-protocol-and-inventory-missing-standardized-protocol
      "{
          PP as var(func: eq(name, \"PatternParticipant\")){}
          SERVICE as var(func: eq(name, \"Service\")) @cascade{
            isA @filter(uid(PP))
          }
          COMM_PROT as var(func: eq(name, \"CommunicationProtocol\")) @cascade{
            isA @filter(uid(PP))
          }
          INV_ROLE as var(func: eq(name, \"Inventory\")) @cascade {
            isA @filter(uid(PP))
          }

          var(func: uid(INV_ROLE)) @cascade {
            INV as ~isA {
              standardizes {
                isA @filter(uid(COMM_PROT))
              }
            }
          }

          var(func: uid(INV_ROLE)) @cascade{
            CANDIDATES as ~isA @filter(not uid(INV)){
              contains {
                I as math(1)
                isA @filter(uid(SERVICE))
              }
              ALL_SERVICES as sum(val(I))
            }
          }

          var(func: uid(CANDIDATES)) @cascade {
            contains {
              isA @filter(uid(SERVICE))
              J as math(1)
              uses {
                isA @filter(uid(COMM_PROT))
              }
            }
            NS_SVRS as sum(val(J))
          }

          var(func: uid(CANDIDATES)) {
            N as math(ALL_SERVICES - NS_SVRS)
          }

          expansions(func: uid(CANDIDATES)) @filter(eq(val(N),0))  @cascade{
            name
            id: _uid_
            contains {
              name
              isA @filter(uid(SERVICE))
              uses {
                isA @filter(uid(COMM_PROT))
                name
                id: _uid_
              }
            }
          }
       }"))