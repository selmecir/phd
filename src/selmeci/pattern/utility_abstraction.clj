(ns selmeci.pattern.utility-abstraction
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.utility-abstraction.vocabulary :as v]
            [selmeci.pattern.utility-abstraction.problems :as problems]
            [selmeci.pattern.utility-abstraction.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->service-delivers-mixed-functionalists)
                   (problems/->service-is-not-marked-as-utility-service)]
        :solutions [(solutions/->utility-abstraction)])))