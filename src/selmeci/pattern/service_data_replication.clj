(ns selmeci.pattern.service-data-replication
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.service-data-replication.vocabulary :as v]
            [selmeci.pattern.service-data-replication.problems :as problems]
            [selmeci.pattern.service-data-replication.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->missing-local-copy-of-db)]
        :solutions [(solutions/->service-data-replication)])))
