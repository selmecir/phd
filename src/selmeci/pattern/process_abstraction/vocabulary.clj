(ns selmeci.pattern.process-abstraction.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :ProcessAbstraction
    #{:Service :TaskService :Functionality :ProcessSpecificFunctionality
      :NonBusinessFunctionality :ProcessAgnosticFunctionality
      :ServiceLayer :TaskServiceLayer
      :BusinessProcess}
    #{[:ProcessSpecificFunctionality :Functionality]
      [:NonBusinessFunctionality :Functionality]
      [:ProcessAgnosticFunctionality :Functionality]
      [:TaskService :Service]
      [:TaskServiceLayer :ServiceLayer]}
    #{:delivers :isDeliveredBy
      :contains :isStoredIn
      :establishes :isEstablishedBy
      :belongsTo :consistsOf}
    #{[:Service :delivers :Functionality]
      [:Functionality :isDeliveredBy :Service]
      [:Service :isStoredIn :ServiceLayer]
      [:ServiceLayer :contains :Service]
      [:ProcessSpecificFunctionality :belongsTo :BusinessProcess]
      [:BusinessProcess :consistsOf :ProcessSpecificFunctionality]}))
