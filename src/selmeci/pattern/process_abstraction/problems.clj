(ns selmeci.pattern.process-abstraction.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->task-service-is-not-in-layer
  []
  (pv/->pattern-variant
    :ProcessAbstraction
    :task-service-is-not-in-layer
    "MATCH (bp:BusinessProcess)-[:consistsOf]->(f:Functionality)
     WITH collect(DISTINCT f) AS functions, bp
     MATCH (bp:BusinessProcess)-[:consistsOf]->(f:Functionality)-[:isDeliveredBy]->(s:Service)
     WHERE NOT (s)-[:isStoredIn]->(:ServiceLayer)
     WITH bp, s, functions, collect(DISTINCT f) AS serviceFunctions
     WHERE  serviceFunctions = functions
     WITH bp, count(DISTINCT s) AS services, s AS service
     WHERE services = 1
     RETURN bp AS businessProcess, service"))

(defn ->business-process-is-scattered
  []
  (pv/->pattern-variant
    :ProcessAbstraction
    :business-process-is-scattered
    "MATCH (bp:BusinessProcess)-[:consistsOf]->(:Functionality)-[:isDeliveredBy]->(s:Service)
     WITH bp, count(DISTINCT s) AS services
     WHERE services > 1
     RETURN bp AS businessProcess"))
