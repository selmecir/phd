(ns selmeci.pattern.process-abstraction.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->process-abstraction
  []
  (pv/->pattern-variant
    :ProcessAbstraction
    :process-abstraction
    "MATCH (bp:BusinessProcess)-[:consistsOf]->(f:Functionality)-[:isDeliveredBy]->(s:Service)
     WITH bp, count(DISTINCT s) AS services, s AS service
     WHERE services = 1 AND
	         (s)-[:isStoredIn]->(:ServiceLayer)
     RETURN bp AS businessProcess, service"))