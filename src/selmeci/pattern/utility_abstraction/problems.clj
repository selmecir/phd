(ns selmeci.pattern.utility-abstraction.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->service-delivers-mixed-functionalists
  []
  (pv/->pattern-variant
    :UtilityAbstraction
    :service-delivers-mixed-functionalists
    "MATCH (s:Service)-[:delivers]->(:NonBusinessFunctionality)
     WHERE (s)-[:delivers]->(:ProcessSpecificFunctionality) OR
	         (s)-[:delivers]->(:ProcessAgnosticFunctionality)
     RETURN s as service"))

(defn ->service-is-not-marked-as-utility-service
  []
  (pv/->pattern-variant
    :UtilityAbstraction
    :service-is-not-marked-as-utility-service
    "MATCH (s:Service)-[:delivers]->(:NonBusinessFunctionality)
     WHERE NOT ((s)-[:delivers]->(:ProcessSpecificFunctionality) OR
	              (s)-[:delivers]->(:ProcessAgnosticFunctionality))
           AND NOT (s:UtilityService)
     RETURN s as service"))