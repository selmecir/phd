(ns selmeci.pattern.utility-abstraction.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :UtilityAbstraction
    #{:Service :UtilityService :Functionality :ProcessSpecificFunctionality
      :NonBusinessFunctionality :ProcessAgnosticFunctionality
      :ServiceLayer :UtilityServiceLayer
      :UtilityServiceModel :ServiceModel}
    #{[:ProcessSpecificFunctionality :Functionality]
      [:NonBusinessFunctionality :Functionality]
      [:ProcessAgnosticFunctionality :Functionality]
      [:UtilityService :Service]
      [:UtilityServiceLayer :ServiceLayer]
      [:UtilityServiceModel :ServiceModel]}
    #{:delivers :isDeliveredBy
      :contains :isStoredIn
      :models :isModeledWith
      :establishes :isEstablishedBy}
    #{[:Service :delivers :Functionality]
      [:Functionality :isDeliveredBy :Service]
      [:Service :isStoredIn :ServiceLayer]
      [:ServiceLayer :contains :Service]
      [:Service :isModeledWith :ServiceModel]
      [:ServiceModel :models :Service]
      [:ServiceLayer :isEstablishedBy :ServiceModel]
      [:ServiceModel :establishes :ServiceLayer]}))
