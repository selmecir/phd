(ns selmeci.pattern.utility-abstraction.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->utility-abstraction
  []
  (pv/->pattern-variant
    :UtilityAbstraction
    :utility-abstraction
    "MATCH (s:UtilityService)-[:isStoredIn]->(:UtilityServiceLayer)
     WHERE NOT ((s)-[:delivers]->(:ProcessSpecificFunctionality) OR
	              (s)-[:delivers]->(:ProcessAgnosticFunctionality))
     RETURN s AS service"))
