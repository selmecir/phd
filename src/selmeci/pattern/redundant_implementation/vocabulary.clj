(ns selmeci.pattern.redundant-implementation.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :RedundantImplementation
    #{:Service :ServiceInstance}
    #{}
    #{:runs :isRunBy
      :communicatesWith}
    #{[:Service :isRunBy :ServiceInstance]
      [:ServiceInstance :runs :Service]
      [:Service :communicatesWith :Service]}))
