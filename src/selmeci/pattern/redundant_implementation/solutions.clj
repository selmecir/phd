(ns selmeci.pattern.redundant-implementation.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->redundant-implementation
  []
  (pv/->pattern-variant
    :RedundantImplementation
    :redundant-implementation
    "MATCH (si:ServiceInstance)-[:runs]->(s:Service)-[:communicatesWith]->(c:Service)
     WITH count(DISTINCT c) AS consumers,
          count(DISTINCT si) AS serviceInstances,
          s
     WHERE consumers > 1 AND  serviceInstances > 1
     RETURN s AS service"))
