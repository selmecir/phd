(ns selmeci.pattern.redundant-implementation.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->missing-redundant-instance
  []
  (pv/->pattern-variant
    :RedundantImplementation
    :missing-redundant-instance
    "MATCH (si:ServiceInstance)-[:runs]->(s:Service)-[:communicatesWith]->(c:Service)
     WITH count(DISTINCT c) AS consumers,
          count(DISTINCT si) AS serviceInstances,
          s
     WHERE consumers > 1 AND  serviceInstances < 2
     RETURN s AS service"))
