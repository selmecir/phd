(ns selmeci.pattern.trusted-subsystem
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.trusted-subsystem.vocabulary :as v]
            [selmeci.pattern.trusted-subsystem.problems :as problems]
            [selmeci.pattern.trusted-subsystem.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->service-resource-is-accessed-by-consumer)
                   (problems/->service-access-resource-without-credentials)]
        :solutions [(solutions/->trusted-subsystem)])))