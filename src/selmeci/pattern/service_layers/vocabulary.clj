(ns selmeci.pattern.service-layers.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :ServiceLayer
    #{:Inventory :Service :ServiceLayer :ServiceModel}
    #{}
    #{:contains :isStoredIn
      :models :isModeledWith
      :establishes :isEstablishedBy}
    #{[:ServiceLayer :isStoredIn :Inventory]
      [:Inventory :contains :ServiceLayer]
      [:Service :isStoredIn :ServiceLayer]
      [:ServiceLayer :contains :Service]
      [:Service :isModeledWith :ServiceModel]
      [:ServiceModel :models :Service]
      [:ServiceLayer :isEstablishedBy :ServiceModel]
      [:ServiceModel :establishes :ServiceLayer]}))
