(ns selmeci.pattern.service-layers.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->service-layers
  []
  (pv/->pattern-variant
    :ServiceLayers
    :service-layers
    "MATCH (i:Inventory)-[:contains]->(s:Service)
     WITH i, count(DISTINCT s) AS allServices
     MATCH (i)-[:contains]->(s:Service)-[:isStoredIn]->(:ServiceLayer)-[:isStoredIn]->(i)
     WITH i, allServices, count(DISTINCT s) AS servicesInLayers
     WHERE allServices = servicesInLayers
     RETURN i as inventory"))
