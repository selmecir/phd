(ns selmeci.pattern.service-layers.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->service-layers-are-not-defined-in-inventory
  []
  (pv/->pattern-variant
    :ServiceLayers
    :service-layers-are-not-defined-in-inventory
    "MATCH (i:Inventory)
     OPTIONAL MATCH (i)-[:contains]->(sl:ServiceLayer)
     WITH count(DISTINCT sl) AS serviceLayers, i
     WHERE serviceLayers < 2
     RETURN i as inventory"))

(defn ->services-in-layer-use-different-service-model
  []
  (pv/->pattern-variant
    :ServiceLayers
    :services-in-layer-use-different-service-model
    "MATCH (sl:ServiceLayer)-[:contains]->(:Service)-[:isModeledWith]->(sm:ServiceModel)
     WITH count(DISTINCT sm) as models, sl
     WHERE models > 1
     RETURN sl AS serviceLayer"))

(defn ->inventory-contains-service-without-service-layer
  []
  (pv/->pattern-variant
    :ServiceLayers
    :inventory-contains-service-without-service-layer
    "MATCH (i:Inventory)-[:contains]->(s:Service)
     WHERE NOT (s)-[:isStoredIn]->(:ServiceLayer)
     RETURN i"))
