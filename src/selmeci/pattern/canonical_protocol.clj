(ns selmeci.pattern.canonical-protocol
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.canonical-protocol.vocabulary :as v]
            [selmeci.pattern.canonical-protocol.problems :as problems]
            [selmeci.pattern.canonical-protocol.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->missing-standardized-communication-protocol)
                   (problems/->services-in-inventory-do-not-use-standardized-protocol)
                   (problems/->all-services-in-inventory-use-other-protocol)
                   (problems/->all-services-in-inventory-use-one-protocol-and-inventory-missing-standardized-protocol)]
        :solutions [(solutions/->services-in-inventory-use-standardized-protocol)])))
