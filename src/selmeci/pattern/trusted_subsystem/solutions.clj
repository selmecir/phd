(ns selmeci.pattern.trusted-subsystem.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->trusted-subsystem
  []
  (pv/->pattern-variant
    :TrustedSubsystem
    :trusted-subsystem
    "MATCH (s:Service)-[:access]->(sr:ServiceResource)-[:isSecuredBy]->(rc:ResourceCredentials)
     WHERE (s)-[:owns]->(rc)
     RETURN s AS service, sr AS resourceService, rc as resourceCredentials"))
