(ns selmeci.pattern.trusted-subsystem.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :TrustedSubsystem
    #{:ServiceResource :TrustBoundary :Consumer :Service :ResourceCredentials}
    #{}
    #{:contains :belongsTo
      :access :isAccessedBy
      :communicatesWith
      :owns :isOwnedBy
      :secures :isSecuredBy}
    #{[:ServiceResource :belongsTo :TrustBoundary]
      [:TrustBoundary :contains :ServiceResource]
      [:Service :belongsTo :TrustBoundary]
      [:TrustBoundary :contains :Service]
      [:Consumer :access :ServiceResource]
      [:ServiceResource :isAccessedBy :Consumer]
      [:Service :access :ServiceResource]
      [:ServiceResource :isAccessedBy :Service]
      [:Service :owns :ResourceCredentials]
      [:ResourceCredentials :isOwnedBy :Service]
      [:ResourceCredentials :secures :ServiceResource]
      [:ServiceResource :isSecuredBy :ResourceCredentials]}))
