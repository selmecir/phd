(ns selmeci.pattern.trusted-subsystem.problems
  (:require [selmeci.pattern-variant.core :as pv]))


(defn ->service-resource-is-accessed-by-consumer
  []
  (pv/->pattern-variant
    :TrustedSubsystem
    :service-resource-is-accessed-by-consumer
    "MATCH (c:Consumer)-[:access]->(sr:ServiceResource) RETURN c as consumer"))

(defn ->service-access-resource-without-credentials
  []
  (pv/->pattern-variant
    :TrustedSubsystem
    :service-access-resource-without-credentials
    "MATCH (s:Service)-[:access]->(sr:ServiceResource)-[:isSecuredBy]->(rc:ResourceCredentials)
     WHERE NOT (s)-[:owns]->(rc)
     RETURN s AS service, sr AS resourceService, rc as resourceCredentials"))