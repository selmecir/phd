(ns selmeci.pattern.schema-centralization.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->data-model-in-several-schemas
  []
  (pv/->pattern-variant
    :SchemaCentralization
    :data-model-in-several-schemas
    "MATCH (dm:DataModel)-[:isDescribedBy]->(ds:DataSchema)
     WITH dm, count(DISTINCT ds) AS dataSchemas
     WHERE dataSchemas > 1
     RETURN dm AS dataModel"))
