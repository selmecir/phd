(ns selmeci.pattern.schema-centralization.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :SchemaCentralization
    #{:ServiceContract :DataModel :DataSchema}
    #{}
    #{:describes :isDescribedBy
      :uses :isUsedBy}
    #{[:ServiceContract :uses :DataSchema]
      [:DataSchema :isUsedBy :ServiceContract]
      [:DataSchema :describes :DataModel]
      [:DataSchema :isDescribedBy :DataModel]}))
