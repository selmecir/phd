(ns selmeci.pattern.schema-centralization.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->schema-centralization
  []
  (pv/->pattern-variant
    :SchemaCentralization
    :schema-centralization
    "MATCH (dm:DataModel)-[:isDescribedBy]->(ds:DataSchema)
     WITH dm, count(DISTINCT ds) AS dataSchemas
     WHERE dataSchemas = 1
     RETURN dm AS dataModel"))