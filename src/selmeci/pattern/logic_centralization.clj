(ns selmeci.pattern.logic-centralization
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.logic-centralization.vocabulary :as v]
            [selmeci.pattern.logic-centralization.problems :as problems]
            [selmeci.pattern.logic-centralization.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->function-logic-is-implemented-in-different-services)
                   (problems/->logic-centralization-candidate)]
        :solutions [(solutions/->logic-centralization)])))
