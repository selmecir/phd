(ns selmeci.pattern.redundant-implementation
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.service-data-replication.vocabulary :as v]
            [selmeci.pattern.redundant-implementation.problems :as problems]
            [selmeci.pattern.redundant-implementation.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->missing-redundant-instance)]
        :solutions [(solutions/->redundant-implementation)])))
