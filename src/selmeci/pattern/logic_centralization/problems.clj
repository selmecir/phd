(ns selmeci.pattern.logic-centralization.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->function-logic-is-implemented-in-different-services
  []
  (pv/->pattern-variant
    :LogicCentralization
    :function-logic-is-implemented-in-different-services
    "MATCH (fl:FunctionLogic)-[:consistsOf]->(:Functionality)-[:isDeliveredBy]->(s:Service)
     WITH fl, count(DISTINCT s) AS services
     WHERE services > 1
     RETURN fl as functionLogic"))

(defn ->logic-centralization-candidate
  []
  (pv/->pattern-variant
    :LogicCentralization
    :logic-centralization-candidate
    "MATCH (fl:FunctionLogic)-[:consistsOf]->(:Functionality)-[:isDeliveredBy]->(s:Service)
     WITH fl, count(DISTINCT s) AS services
     WHERE services = 1 AND
	         NOT (:Service)-[:isOfficialFor]->(fl)
     RETURN fl as functionLogic"))