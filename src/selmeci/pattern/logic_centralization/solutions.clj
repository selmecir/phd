(ns selmeci.pattern.logic-centralization.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->logic-centralization
  []
  (pv/->pattern-variant
    :LogicCentralization
    :logic-centralization
    "MATCH (fl:FunctionLogic)-[:consistsOf]->(:Functionality)-[:isDeliveredBy]->(s:Service)
     WITH fl, count(DISTINCT s) AS services
     WHERE services = 1 AND
	         (:Service)-[:isOfficialFor]->(fl)
     RETURN fl as functionLogic"))