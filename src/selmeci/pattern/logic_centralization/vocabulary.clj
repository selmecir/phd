(ns selmeci.pattern.logic-centralization.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :LogicCentralization
    #{:Service :Functionality :FunctionLogic}
    #{}
    #{:delivers :isDeliveredBy
      :belongsTo :consistsOf
      :isOfficialFor :restricts}
    #{[:Service :delivers :Functionality]
      [:Functionality :isDeliveredBy :Service]
      [:Functionality :belongsTo :FunctionLogic]
      [:FunctionLogic :consistsOf :Functionality]
      [:Service :isOfficialFor :FunctionLogic]
      [:FunctionLogic :restricts :Service]}))
