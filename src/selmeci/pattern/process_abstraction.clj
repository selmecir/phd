(ns selmeci.pattern.process-abstraction
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.process-abstraction.vocabulary :as v]
            [selmeci.pattern.process-abstraction.problems :as problems]
            [selmeci.pattern.process-abstraction.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->task-service-is-not-in-layer)
                   (problems/->business-process-is-scattered)]
        :solutions [(solutions/->process-abstraction)])))