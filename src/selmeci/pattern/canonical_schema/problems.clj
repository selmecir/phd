(ns selmeci.pattern.canonical-schema.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->data-model-transformation-is-need
  []
  (pv/->pattern-variant
    :CanonicalSchema
    :data-model-transformation-is-need
    "MATCH (s1:Service)-[:communicatesWith]->(s2:Service),
	         (s1)-[:produces]->(d:Data)<-[:consumes]-(s2),
           (s1)-[:uses]->(dm1:DataModel)-[:definesStructureFor]->(d),
           (s2)-[:uses]->(dm2:DataModel)-[:definesStructureFor]->(d)
     WHERE NOT dm1 = dm2 AND
	         NOT (dm1)<-[:transformsTo|:transformsFrom]-(:DataModelTransformation)-[:transformsFrom|:transformsTo]->(dm2)
     RETURN s1 as service1, s2 as service2,dm1 as dataModel1, dm2 as dataModel2,d as data"))

(defn ->data-model-transformation-exists
  []
  (pv/->pattern-variant
    :CanonicalSchema
    :data-model-transformation-exists
    "MATCH (s1:Service)-[:communicatesWith]->(s2:Service),
	         (s1)-[:produces]->(d:Data)<-[:consumes]-(s2),
           (s1)-[:uses]->(dm1:DataModel)-[:definesStructureFor]->(d),
           (s2)-[:uses]->(dm2:DataModel)-[:definesStructureFor]->(d),
           (dm1)<-[:transformsTo|:transformsFrom]-(t:DataModelTransformation)-[:transformsFrom|:transformsTo]->(dm2)
     WHERE NOT dm1 = dm2
     RETURN s1 as service1, s2 as service2,dm1 as dataModel1, dm2 as dataModel2,d as data, t as transformation"))
