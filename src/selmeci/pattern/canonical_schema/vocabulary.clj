(ns selmeci.pattern.canonical-schema.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :CanonicalSchema
    #{:Service :Data :DataModel :DataModelTransformation}
    #{}
    #{:communicatesWith
      :uses :isUsedBy
      :implements :isImplementedBy
      :transformsTo :transformsFrom
      :isTransformedBy
      :definesStructureFor :isStructuredWith
      :produces :isProducedBy
      :consumes :isConsumedBy}
    #{[:Service :communicatesWith :Service]
      [:Service :produces :Data]
      [:Data :isProducedBy :Service]
      [:Service :consumes :Data]
      [:Data :isConsumedBy :Service]
      [:Service :uses :DataModel]
      [:DataModel :isUsedBy :Service]
      [:DataModel :definesStructureFor :Data]
      [:Data :isStructuredWith :DataModel]
      [:DataModel :isTransformedBy :DataModelTransformation]
      [:DataModelTransformation :transformsTo :DataModel]
      [:DataModelTransformation :transformsFrom :DataModel]
      [:DataModelTransformation :isImplementedBy :Service]
      [:Service :implements :DataModelTransformation]}))
