(ns selmeci.pattern.canonical-schema.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->canonical-schema-is-used
  []
  (pv/->pattern-variant
    :CanonicalSchema
    :canonical-schema-is-used
    "MATCH (s1:Service)-[:communicatesWith]->(s2:Service),
	         (s1)-[:produces]->(d:Data)<-[:consumes]-(s2),
           (dm:DataModel)-[:definesStructureFor]->(d),
           (s2)-[:uses]->(dm)<-[:uses]-(s1)
     RETURN s1 as service1, s2 as service2,dm as dataModel1, d as data"))
