(ns selmeci.pattern.entity-abstraction.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->service-delivers-functionality-from-different-business-entities
  []
  (pv/->pattern-variant
    :EntityAbstraction
    :service-delivers-functionality-from-different-business-entities
    "MATCH (s:Service)-[:delivers]->(:ProcessAgnosticFunctionality)-[:belongsTo]->(be:BusinessEntity)
     WITH count(DISTINCT be) AS businessEntities, s
     WHERE businessEntities > 1
     RETURN s AS service"))

(defn ->entity-service-is-not-entity-layer
  []
  (pv/->pattern-variant
    :EntityAbstraction
    :entity-service-is-not-entity-layer
    "MATCH (s:Service)-[:delivers]->(:ProcessAgnosticFunctionality)-[:belongsTo]->(be:BusinessEntity)
     WITH count(DISTINCT be) AS businessEntities, s
     WHERE businessEntities = 1 AND
           NOT (s)-[:isStoredIn]->(:EntityServiceLayer)
     RETURN s AS service"))
