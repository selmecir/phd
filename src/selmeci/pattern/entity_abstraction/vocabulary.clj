(ns selmeci.pattern.entity-abstraction.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :EntityAbstraction
    #{:Service :EntityService :Functionality :ProcessSpecificFunctionality
      :NonBusinessFunctionality :ProcessAgnosticFunctionality
      :ServiceLayer :EntityServiceLayer :BusinessEntity
      :EntityServiceModel :ServiceModel}
    #{[:ProcessSpecificFunctionality :Functionality]
      [:NonBusinessFunctionality :Functionality]
      [:ProcessAgnosticFunctionality :Functionality]
      [:EntityService :Service]
      [:EntityServiceLayer :ServiceLayer]
      [:EntityServiceModel :ServiceModel]}
    #{:delivers :isDeliveredBy
      :contains :isStoredIn
      :models :isModeledWith
      :establishes :isEstablishedBy
      :belongsTo :defines}
    #{[:Service :delivers :Functionality]
      [:Functionality :isDeliveredBy :Service]
      [:Service :isStoredIn :ServiceLayer]
      [:ServiceLayer :contains :Service]
      [:Service :isModeledWith :ServiceModel]
      [:ServiceModel :models :Service]
      [:ServiceLayer :isEstablishedBy :ServiceModel]
      [:ServiceModel :establishes :ServiceLayer]
      [:ProcessAgnosticFunctionality :belongsTo :BusinessEntity]
      [:BusinessEntity :defines :ProcessAgnosticFunctionality]}))
