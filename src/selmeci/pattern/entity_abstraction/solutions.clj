(ns selmeci.pattern.entity-abstraction.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->entity-abstraction
  []
  (pv/->pattern-variant
    :EntityAbstraction
    :entity-abstraction
    "MATCH (s:Service)-[:delivers]->(:ProcessAgnosticFunctionality)-[:belongsTo]->(be:BusinessEntity)
     WITH count(DISTINCT be) AS businessEntities, s
     WHERE businessEntities = 1 AND
           (s)-[:isStoredIn]->(:EntityServiceLayer)
     RETURN s AS service"))