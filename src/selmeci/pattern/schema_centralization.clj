(ns selmeci.pattern.schema-centralization
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.schema-centralization.vocabulary :as v]
            [selmeci.pattern.schema-centralization.problems :as problems]
            [selmeci.pattern.schema-centralization.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->data-model-in-several-schemas)]
        :solutions [(solutions/->schema-centralization)])))
