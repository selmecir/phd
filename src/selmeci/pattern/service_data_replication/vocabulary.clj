(ns selmeci.pattern.service-data-replication.vocabulary
  (:require [selmeci.pattern-vocabulary.core :as pv]))

(defn ->vocabulary
  "Return component for pattern vocabulary"
  []
  (pv/->PatternVocabulary
    :ServiceDataReplication
    #{:Service :ServiceInstance :Server :Database}
    #{[:Database :Service]
      [:Database :InfrastructureService]
      [:InfrastructureService :Service]}
    #{:runs :isRunBy
      :deploys :isDeployedOn
      :worksWith :isUsedBy
      :stores :isStoredIn
      :provides :isProvidedBy}
    #{[:Service :isRunBy :ServiceInstance]
      [:ServiceInstance :runs :Service]
      [:ServiceInstance :isDeployedOn :Server]
      [:Server :deploys :ServiceInstance]
      [:Service :provides :Database]
      [:Database :isProvidedBy :Service]
      [:Service :worksWith :Database]
      [:Database :isUsedBy :Service]
      [:Server :stores :Database]
      [:Database :isStoredIn :Server]}))
