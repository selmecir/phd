(ns selmeci.pattern.service-data-replication.problems
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->missing-local-copy-of-db
  []
  (pv/->pattern-variant
    :ServiceDataReplication
    :missing-local-copy-of-db
    "MATCH (db:Database)-[:isUsedBy]->(:Service)-[:isRunBy]->(si:ServiceInstance)-[:isDeployedOn]->(server:Server)
     WHERE NOT (db)-[:isStoredIn]->(server)
     RETURN db as database, si as service"))

#_(defn ->missing-local-copy-of-db
    []
    (pv/->pattern-variant
      :ServiceDataReplication
      :missing-local-copy-of-db
    "{
        PP as var(func: eq(name, \"PatternParticipant\")){}
        var(func: eq(name, \"Database\")) @cascade {
          isA @filter(uid(PP))
          DBS as ~isA {
            DB_SERVERS as isStoredIn
          }
        }

        PROBLEMS as var(func: uid(DBS)) @cascade{
          db_uid: _uid_
          name: name
          isUsedBy {
            isRunBy {
              isDeployedOn @filter(not uid(DB_SERVERS)){
                server_name: name
                server_id: _uid_
              }
            }
          }
        }

        expansions(func: uid(PROBLEMS)) @cascade {
          name
          db_uid: _uid_
          isStoredIn {
            name
          }
          isUsedBy {
            name
            isRunBy {
              name
              isDeployedOn {
                name
              }
            }
          }
        }
     }"))
