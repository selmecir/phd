(ns selmeci.pattern.service-data-replication.solutions
  (:require [selmeci.pattern-variant.core :as pv]))

(defn ->service-data-replication
  []
  (pv/->pattern-variant
    :ServiceDataReplication
    :solution
    "MATCH (db:Database)-[:isUsedBy]->(:Service)-[:isRunBy]->(:ServiceInstance)-[:isDeployedOn]->(instanceServer:Server),
           (db)-[:isStoredIn]->(dbServer:Server)
     WITH collect(DISTINCT instanceServer) AS instanceServers,
          collect(DISTINCT dbServer) AS dbServers,
          db
     WHERE instanceServers = dbServers
     RETURN db as database"))

#_(defn ->service-data-replication
    []
    (pv/->pattern-variant
      :ServiceDataReplication
      :solution
    "{
        PP as var(func: eq(name, \"PatternParticipant\")){}
        var(func: eq(name, \"Database\")) @cascade {
          isA @filter(uid(PP))
          DBS as ~isA {
            DB_SERVERS as isStoredIn
          }
        }

        PROBLEMS as var(func: uid(DBS)) @cascade{
          db_uid: _uid_
          name: name
          isUsedBy {
            isRunBy {
              isDeployedOn @filter(not uid(DB_SERVERS)){
                server_name: name
                server_id: _uid_
              }
            }
          }
        }

      	SOLUTIONS as var (func: uid(DBS)) @filter (not uid(PROBLEMS)){}

        expansions(func: uid(SOLUTIONS)) @cascade{
          name
          db_uid: _uid_
          isStoredIn {
            name
          }
          isUsedBy {
            name
            isRunBy {
              name
              isDeployedOn {
                name
              }
            }
          }
        }
     }"))
