(ns selmeci.pattern.service-layers
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.service-layers.vocabulary :as v]
            [selmeci.pattern.service-layers.problems :as problems]
            [selmeci.pattern.service-layers.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->service-layers-are-not-defined-in-inventory)
                   (problems/->services-in-layer-use-different-service-model)
                   (problems/->inventory-contains-service-without-service-layer)]
        :solutions [(solutions/->service-layers)])))
