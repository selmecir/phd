(ns selmeci.pattern.canonical-schema
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.canonical-schema.vocabulary :as v]
            [selmeci.pattern.canonical-schema.problems :as problems]
            [selmeci.pattern.canonical-schema.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->data-model-transformation-is-need)
                   (problems/->data-model-transformation-exists)]
        :solutions [(solutions/->canonical-schema-is-used)])))
