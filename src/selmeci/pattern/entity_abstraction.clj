(ns selmeci.pattern.entity-abstraction
  (:require [selmeci.pattern.core :as p]
            [com.stuartsierra.component :as component]
            [selmeci.pattern.entity-abstraction.vocabulary :as v]
            [selmeci.pattern.entity-abstraction.problems :as problems]
            [selmeci.pattern.entity-abstraction.solutions :as solutions]))

(defn ->pattern
  "Return component for pattern"
  []
  (-> (component/start
        (component/system-map
          :vocabulary (v/->vocabulary)))
      (assoc
        :problems [(problems/->service-delivers-functionality-from-different-business-entities)
                   (problems/->entity-service-is-not-entity-layer)]
        :solutions [(solutions/->entity-abstraction)])))
