(ns selmeci.pattern-variant.detection
  (:require [clojure.spec.alpha :as s]
            [com.stuartsierra.component :as component]
            [com.edocu.help.db.neo4j :as neo4j]
            [taoensso.timbre :as log]
            [clojure.core.async :as async :refer [close! <!! >! <! chan put! go]]))

(defprotocol IDectection
  (detect [this] "Return channel with all detected variants"))

(defrecord PatternVariantDetector [client name variants detections]
  component/Lifecycle
  (start [this]
    (log/trace "Start PatternVariantDetector: " name)
    (assoc this
      :detections (detect this)))

  (stop [this]
    (log/trace "Stop PatternVariantDetector: " name)
    this)

  IDectection
  (detect [this]
    (let [<queries (async/merge
                     (sequence
                       (comp
                         (filter seq)
                         (map
                           (fn [{:keys [pattern name query] :as variant}]
                             (log/trace "detection. pattern:" pattern "variant:" name)
                             (go {:variant   variant
                                  :detection (<! (async/into
                                                   []
                                                   (neo4j/<statement-run client query)))}))))
                       variants))]
      (<!!
        (async/reduce
          (fn [tmp {:keys [variant detection] :as result}]
            (if (seq detection)
              (assoc tmp
                variant
                detection)
              tmp))
          {}
          <queries)))))

(defn ->pattern-variant-detector
  "Return compoent for pattern variant"
  [name variants]
  (map->PatternVariantDetector {:name     name
                                :variants variants}))