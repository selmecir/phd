(ns selmeci.pattern-variant.core
  (:require [clojure.spec.alpha :as s]
            [selmeci.pattern-vocabulary.core :as pv]
            [com.stuartsierra.component :as component]
            [selmeci.help.dgraph :as dg]
            [taoensso.timbre :as log]))

(s/def ::id keyword?)
(s/def ::ids (s/coll-of ::id :min-count 1 :distinct true :kind set?))
(s/def ::participant (s/keys :req [::pv/role ::id]))
(s/def ::participants (s/coll-of ::participant :min-count 1 :distinct true :kind set?))
(s/def ::participants-roles (s/map-of ::id ::pv/role))
(s/def ::relation (s/cat :subject ::id
                         :predicate ::pv/relation-role
                         :object ::id))
(s/def ::relations (s/coll-of ::relation :min-count 1 :distinct true :kind set?))

(defn- extract-participants-ids
  [participants]
  (into
    #{}
    (map ::id participants)))

(s/fdef extract-participants-ids
        :args (s/cat :participants ::participants)
        :ret ::ids)

(defn- subject-and-object-in-relation-belongs-to-participants?
  [participants-ids [subject _ object]]
  (and (participants-ids subject)
       (participants-ids object)))

(s/fdef subject-and-object-in-relation-belongs-to-participants?
        :args (s/cat :participants-ids ::ids
                     :relations ::pv/relation)
        :ret boolean?)

(defn valid-participants-in-pattern-variant?
  "Check if all participants in pattern variant relations are defined"
  [participants relations]
  (let [ids (extract-participants-ids participants)]
    (every?
      #(subject-and-object-in-relation-belongs-to-participants? ids %)
      relations)))

(s/fdef valid-participants-in-pattern-variant?
        :args (s/cat :participants ::participants
                     :relations ::relations)
        :ret boolean?)

(defn- participants->participants-roles
  "Return hash map with participant id assigned to role"
  [participants]
  (into
    {}
    (map
      (fn [participant]
        [(::id participant)
         (::pv/role participant)])
      participants)))

(s/fdef participants->participants-roles
        :args (s/cat :participants ::participants)
        :ret ::participants-roles)

(defn- pattern-variant-relations->pattern-vocabulary-relations
  "Extract pattern variants relations into vocabulary relations"
  [participants-roles relations]
  (into
    #{}
    (map
      (fn [{:keys [subject predicate object] :as t}]
        {:subject   (subject participants-roles)
         :predicate predicate
         :object    (object participants-roles)})
      relations)))

(s/fdef pattern-variant-relations->pattern-vocabulary-relations
        :args (s/cat :participants-roles ::participants-roles
                     :relations ::relations)
        :ret ::pv/relations)

(defn- valid-relations-in-pattern-variant?
  "Check if all relations belong to pattern vocabulary"
  [pattern-variant]
  (let [participants-roles (participants->participants-roles
                             (::participants pattern-variant))
        participant-relations (pattern-variant-relations->pattern-vocabulary-relations
                                participants-roles
                                (::relations pattern-variant))
        vocabulary-relations (get-in pattern-variant [::pv/patter-vocabulary ::pv/relations])]
    (every?
      #(vocabulary-relations %)
      participant-relations)))

(s/fdef valid-relations-in-pattern-variant?
        :args (s/cat :pattern-variant ::pattern-variant)
        :ret boolean?)

(s/def ::pattern-variant
  (s/and (s/keys :req [::pv/patter-vocabulary
                       ::participants
                       ::relations])
         #(valid-relations-in-pattern-variant? %)))

(defrecord PatternVariant [pattern name query]
  component/Lifecycle
  (start [this]
    (log/trace "Start PatternVariant: " name)
    this)

  (stop [this]
    (log/trace "Stop PatternVariant: " name)
    this))

(defn ->pattern-variant
  "Return compoent for pattern variant"
  [pattern name query]
  (map->PatternVariant {:name    name
                        :pattern pattern
                        :query   query}))
