(ns selmeci.canvas.core)

(def ^:const PARTICIPANT "Participant")

(defprotocol ICanvas
  (add-model! [canvas model]
    "Added model to canvas")
  (models-register [canvas]
    "Return models register"))