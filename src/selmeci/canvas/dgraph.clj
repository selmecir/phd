(ns selmeci.canvas.dgraph
  (:require [com.stuartsierra.component :as component]
            [clojure.core.async :refer [<!!]]
            [taoensso.timbre :as log]
            [selmeci.pattern-variant.core :as patter-variant]
            [selmeci.pattern-vocabulary.core :as pattern-vocabulary]
            [clojure.string :as clj-str]
            [selmeci.help.dgraph :as dg]))

(defn- extract-participant-roles
  "Return set of participants roles"
  [participants]
  (->> participants
       (map ::pattern-vocabulary/role)
       (reduce
         (fn [tmp role]
           (if (keyword? role)
             (conj tmp role)
             (into tmp role)))
         #{})
       (map name)
       set))

(defn- role->pattern-participant-query
  "Return query for pattern participant with given role"
  [role]
  (format
    "%s as var(func: eq(name, \"%s\")) @cascade {
        name
        isA @filter(eq(name, \"PatternParticipant\"))
     }"
    role
    role))

(defn- roles->dgraph-query
  "Return query with roles as variables"
  [roles]
  (format
    "{
      %s
     }"
    (clj-str/join "\n" (map role->pattern-participant-query roles))))

(defn- participant->pattern-participants
  "Assign participant to its pattern participant role"
  [participants]
  (clj-str/join
    "\n"
    (map
      (fn [participant]
        (let [id (::patter-variant/id participant)
              role (::pattern-vocabulary/role participant)]
          (format
            "%s
             _%s <name> \"%s\" ."
            (clj-str/join
              (map #(format "_%s <isA> uid(%s) . \n" id (name %))
                   (if (keyword? role)
                     [role]
                     role)))
            id
            (name id))))
      participants)))

(defn- relations->dgraph-mutation
  "Return command for specify relations in dgraph mutation"
  [relations]
  (clj-str/join
    "\n"
    (map
      (fn [[subject predicate object]]
        (format
          "_%s <%s> _%s ."
          subject
          (name predicate)
          object))
      relations)))

(defn model->dgraph-mutation
  "Convert model to mutation for dGraph DB"
  [{:keys [participants relations] :as model}]
  (let [roles (extract-participant-roles participants)
        roles->dgraph-query (future (roles->dgraph-query roles))
        participant->pattern-participants (future (participant->pattern-participants participants))
        relations->dgraph-mutation (future (relations->dgraph-mutation relations))]
    (format
      "%s
       mutation {
        set {
          %s
          %s
        }
       }"
      @roles->dgraph-query
      @participant->pattern-participants
      @relations->dgraph-mutation)))

(def ^:const clear-dgraph-query
  "{
    pp as var(func: eq(name, \"PatternParticipant\"))
   }
   mutation {
    delete {
      * <isA> * .
      uid(pp) * * .
    }
  }")

(defprotocol ICanvas
  (add-model! [canvas model]
    "Added model to canvas")
  (models-register [canvas]
    "Return models register"))

(defrecord Canvas [dgraph register]
  component/Lifecycle
  (start [this]
    (log/trace "Canvas started")
    (let [canvas (assoc this
                   :register (atom {}))]
      canvas))
  (stop [this]
    (log/trace "Canvas stopped")
    (<!! (dg/<execute-query!
           dgraph
           clear-dgraph-query))
    (dissoc this :register))

  ICanvas
  (add-model! [this {:keys [model-name participants relations] :as model}]
    (assert
      (nil? (model-name @register))
      (format "Model duplicates: %s" model-name))
    (dg/<execute-query!
      dgraph
      (model->dgraph-mutation model))
    (swap!
      register
      assoc
      model-name
      {:participants participants
       :relations    relations}))

  (models-register [this]
    @register))

(defn ->canvas
  "Return component with canvas"
  []
  (map->Canvas {}))
