(ns selmeci.canvas.neo4j
  (:use selmeci.canvas.core)
  (:require [com.stuartsierra.component :as component]
            [clojure.core.async :refer [<!!]]
            [taoensso.timbre :as log]
            [selmeci.pattern-variant.core :as patter-variant]
            [selmeci.pattern-vocabulary.core :as pattern-vocabulary :refer [PATTERN_ROLE]]
            [clojure.string :as clj-str]
            [com.edocu.help.db.neo4j :as neo4j]
            [selmeci.help.utils :as utils]
            [clojure.core.memoize :as memo]))

(def inherited-roles
  (memo/ttl
    (fn [client roles]
      (let [query (format
                    "MATCH (c:%s)-[:isA*]->(r:%s)
                     WHERE c.name in [%s]
                     RETURN collect(DISTINCT r.name) AS roles"
                    PATTERN_ROLE
                    PATTERN_ROLE
                    (clj-str/join "," (map #(format "\"%s\"" %) roles)))]
        (get (<!! (neo4j/<statement-run client query)) "roles")))
    :ttl/threshold (* 1000 30)))

(defn- participants->pattern-roles
  "Assign participants to its pattern role"
  [client participants]
  (mapv
    (fn [participant]
      (let [id (::patter-variant/id participant)
            roles (::pattern-vocabulary/role participant)
            roles (map
                    utils/->PascalCaseString
                    (if (keyword? roles)
                      #{roles}
                      roles))
            roles (into roles (inherited-roles client roles))]
        {:statement (format
                      "CREATE (:%s:%s {name: {id}})"
                      PARTICIPANT
                      (clj-str/join ":" roles))
         :params    {"id" (name id)}}))
    participants))

(defn- relations->cyphers
  "Return vector of cypher query for create relation"
  [relations]
  (mapv
    (fn [[subject predicate object]]
      {:statement (format
                    "MATCH (subject:%s {name: {subject}}),
                           (object:%s {name: {object}})
                     MERGE (subject)-[:%s]->(object)"
                    PARTICIPANT
                    PARTICIPANT
                    (utils/->camelCaseString predicate))
       :params    {"subject" (name subject)
                   "object"  (name object)}})
    relations))

(defn create-model!
  [client {:keys [participants relations] :as model}]
  (let [participant->pattern-participants (future (participants->pattern-roles client participants))
        relations->cyphers (future (relations->cyphers relations))
        statements (concat
                     @participant->pattern-participants
                     @relations->cyphers)]
    (<!! (neo4j/<transaction-run
           client
           statements))))

(def ^:const clear-query
  "MATCH (n) DETACH DELETE n")

(defrecord Canvas [client register]
  component/Lifecycle
  (start [this]
    (log/trace "Canvas started")
    (let [canvas (assoc this
                   :register (atom {}))]
      canvas))
  (stop [this]
    (log/trace "Canvas stopped")
    (<!! (neo4j/<statement-run
           client
           clear-query))
    (dissoc this :register))

  ICanvas
  (add-model! [this {:keys [model-name participants relations] :as model}]
    (assert
      (nil? (model-name @register))
      (format "Model duplicates: %s" model-name))
    (create-model! client model)
    (swap!
      register
      assoc
      model-name
      {:participants participants
       :relations    relations}))

  (models-register [this]
    @register))

(defn ->canvas
  "Return component with canvas"
  []
  (map->Canvas {}))
