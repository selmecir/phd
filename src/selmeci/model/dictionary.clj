(ns selmeci.model.dictionary
  (:require [clojure.spec.alpha :as s]
            [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.pattern-variant.core :as patter-variant]
            [selmeci.pattern-vocabulary.core :as pattern-vocabulary]
            [selmeci.canvas.core :as c]))

(defprotocol DictionaryManager
  (roles-dic [this] "Return map with participant associate to all them roles")
  (relations-dic [this] "Return map with relation assigned to model where participants are replaced with theirs roles"))

(defn- roles-dic-impl
  [models-register]
  (->> models-register
       (reduce
         (fn [dic [_ {:keys [participants]}]]
           (reduce
             (fn [tmp participant]
               (let [role (::pattern-vocabulary/role participant)]
                 (update
                   tmp
                   (::patter-variant/id participant)
                   into
                   (if (keyword? role)
                     [role]
                     role))))
             dic
             participants))
         {})
       (map (fn [[id roles]]
              [id (set roles)]))
       (into {})))

(defn- relations-dic-impl
  [dictionary models-register]
  (let [roles-dic (roles-dic dictionary)]
    (->> models-register
         (reduce
           (fn [dic [model {:keys [relations]}]]
             (reduce
               (fn [tmp [subject relation object]]
                 (update
                   tmp
                   {:relation [subject relation object]
                    :roles    [(subject roles-dic)
                               relation
                               (object roles-dic)]}
                   conj
                   model))
               dic
               relations))
           {})
         (map (fn [[relation models]]
                [relation (set models)]))
         (into {}))))

(defrecord ModelsDictionary [canvas roles-dic-delay relations-dic-delay]
  component/Lifecycle
  (start [this]
    (log/trace "ModelsDictionary started")
    (let [implemented-roles-dic (assoc this :roles-dic-delay (delay (roles-dic-impl (c/models-register canvas))))]
      (assoc implemented-roles-dic
        :relations-dic-delay (delay (relations-dic-impl implemented-roles-dic (c/models-register canvas))))))

  (stop [this]
    (log/trace "ModelsDictionary stopped")
    (dissoc this :roles-dic-delay :relations-dic-delay))

  DictionaryManager
  (roles-dic [this] @roles-dic-delay)
  (relations-dic [this] @relations-dic-delay))

(defn ->model-dictionary
  "Return component for model dictionary"
  []
  (map->ModelsDictionary {}))
