(ns selmeci.model.validation.dgraph
  (:require [clojure.string :as clj-str]
            [clojure.core.async :as async :refer [close! >! <! chan put! pipe go <!!]]
            [clojure.string :as clj-str]
            [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.model.validation :as validation]
            [selmeci.help.dgraph :as dg]))

(defn- ->valid-relation-query
  "Return query for check valid relation among participants"
  [[subjects-roles relation objects-roles]]
  (format "{

    participant as var(func: eq(name, \"PatternParticipant\")){}

    subject as var(func: anyofterms(name, \"%s\")) @cascade {
      isA @filter(uid(participant))
    }

    var(func: uid(subject)){
      possible_subjects as isA @filter(not uid(participant))
    }

    object as var(func: anyofterms(name, \"%s\")) @cascade {
      isA @filter(uid(participant))
    }

    valid(func: uid(subject,possible_subjects)) @cascade {
      name
      %s @filter(uid(object))
    }
  }"
          (clj-str/join " " (map name subjects-roles))
          (clj-str/join " " (map name objects-roles))
          (name relation)))

(defrecord RelationValidator [client]
  component/Lifecycle
  (start [this]
    (log/trace "RelationValidator started")
    this)
  (stop [this]
    (log/trace "RelationValidator stopped")
    this)

  validation/IValidRelation
  (<valid? [this relation]
    (go
      (let [query (->valid-relation-query relation)
            valid (->> (<! (dg/<execute-query!
                             client
                             query))
                       :valid
                       (seq)
                       (some?))]
        valid))))

(defn ->relation-validator
  "Create component for relation validator"
  []
  (map->RelationValidator {}))