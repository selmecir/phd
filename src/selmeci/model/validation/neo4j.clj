(ns selmeci.model.validation.neo4j
  (:require [clojure.core.async :as async :refer [close! >! <! chan put! pipe go <!!]]
            [clojure.string :as clj-str]
            [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [com.edocu.help.db.neo4j :as neo4j]
            [selmeci.model.validation :as validation]
            [selmeci.pattern-vocabulary.core :refer [PATTERN_ROLE]]
            [selmeci.help.utils :refer [->PascalCaseString ->camelCaseString]]))

(defn- ->valid-relation-query
  "Return query for check valid relation among participants"
  [[subjects-roles relation objects-roles]]
  (let [relation (->camelCaseString relation)]
    (format "MATCH (subject:%s),(object:%s)
             WHERE
                subject.name IN [%s] AND
                object.name IN [%s]
             OPTIONAL MATCH (subject)-[:%s]->(object)
             OPTIONAL MATCH (subject)-[:isA]->(possibleSubject:%s)
             WHERE (possibleSubject)-[:%s]->(object)
             RETURN TRUE as true LIMIT 1"
            PATTERN_ROLE
            PATTERN_ROLE
            (clj-str/join "," (map (comp
                                     #(format "\"%s\"" %)
                                     ->PascalCaseString) subjects-roles))
            (clj-str/join "," (map (comp
                                     #(format "\"%s\"" %)
                                     ->PascalCaseString) objects-roles))
            relation
            PATTERN_ROLE
            relation)))

(defrecord RelationValidator [client]
  component/Lifecycle
  (start [this]
    (log/trace "RelationValidator started")
    this)
  (stop [this]
    (log/trace "RelationValidator stopped")
    this)

  validation/IValidRelation
  (<valid? [this relation]
    (let [statement (->valid-relation-query relation)]
      (neo4j/<statement-run client statement))))

(defn ->relation-validator
  "Create component for relation validator"
  []
  (map->RelationValidator {}))
