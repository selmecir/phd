(ns selmeci.model.core
  (:require [clojure.spec.alpha :as s]
            [clojure.core.async :as async :refer [close! >! <! chan put! pipe go <!!]]
            [clojure.string :as clj-str]
            [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.pattern-variant.core :as patter-variant]
            [selmeci.pattern-vocabulary.core :as pattern-vocabulary]
            [selmeci.model.dictionary :as dictionary]
            [selmeci.help.dgraph :as dg]))

(defn- assert-valid-participants-definition
  "Check if participants mod 2 is 0"
  [participants]
  (assert
    (= 0 (mod (count participants) 2))
    "Participants should be defined as category with name and role"))

(defn- assert-participants-roles
  "Check if all roles are defined in pattern vocabulary"
  [entities-roles participants]
  (doseq [[_ role] (partition-all 2 participants)]
    (assert (some? (entities-roles role))
            (format "Participant role is not defined: %s" role))))

(defn- assert-valid-relations-definition
  "Check if relations mod 3 is 0"
  [relations]
  (assert
    (= 0 (mod (count relations) 3))
    "Participants should be defined as category with name and role"))

(defn- assert-relations-roles
  "Check if all relations are defined in pattern vocabulary"
  [relations-roles relations]
  (doseq [[_ relation _] (partition-all 3 relations)]
    (assert (some? (relations-roles relation))
            (format "Relation role is not defined: %s" relation))))

(defprotocol IModel
  (with-participants [this participants]
    "Added participants to model")

  (with-relations [this relations]
    "Added relations to model"))

(defrecord Model [vocabulary model-name participants relations]
  IModel
  (with-relations
    [this rels]
    (assert-valid-relations-definition rels)
    (assert-relations-roles (:relations-roles vocabulary) rels)
    (assoc this
      :relations (into relations (partition-all 3 rels))))

  (with-participants
    [this parts]
    (assert-valid-participants-definition parts)
    (assert-participants-roles (:entities-roles vocabulary) parts)
    (assoc this
      :participants
      (reduce
        (fn [tmp [id role]]
          (conj
            tmp
            {::pattern-vocabulary/role role
             ::patter-variant/id       id}))
        participants
        (partition-all 2 parts)))))

(defn ->model
  "Return model"
  [vocabulary model-name]
  (map->Model {:vocabulary   vocabulary
               :model-name   model-name
               :participants #{}
               :relations    #{}}))

(defn sync-models
  "Sync model by participant IDs"
  [vocabulary model-name model1 model2 & models]
  (loop [m1 model1
         m2 model2
         ms models]
    (let [m1-participants-dic (reduce
                                (fn [tmp participant]
                                  (let [role (::pattern-vocabulary/role participant)]
                                    (assoc
                                      tmp
                                      (::patter-variant/id participant)
                                      (if (set? role)
                                        role
                                        #{role}))))
                                {}
                                (:participants m1))
          participants-dic (reduce
                             (fn [tmp participant]
                               (let [participant-id (::patter-variant/id participant)
                                     role (::pattern-vocabulary/role participant)]
                                 (assoc tmp
                                   participant-id
                                   (if-let [roles (participant-id tmp)]
                                     (conj roles role)
                                     #{role}))))
                             m1-participants-dic
                             (:participants m2))
          merged-model (->Model
                         vocabulary
                         model-name
                         (set
                           (map (fn [[participant-id roles]]
                                  {::patter-variant/id       participant-id
                                   ::pattern-vocabulary/role roles})
                                participants-dic))
                         (set
                           (into (:relations m1)
                                 (:relations m2))))]
      (if (seq ms)
        (recur merged-model
               (first ms)
               (rest ms))
        merged-model))))
