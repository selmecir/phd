(ns selmeci.model.validation
  (:require [clojure.core.async :as async :refer [close! >! <! chan put! pipe go <!!]]
            [clojure.string :as clj-str]
            [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]
            [selmeci.pattern-vocabulary.core :as pattern-vocabulary]
            [selmeci.model.dictionary :as dictionary]
            [clojure.pprint :as clj-pp]))

(defprotocol IValidRelation
  (<valid? [this relation] "Return channel with true if relation is valid"))

(defn- any-invalid-models?
  "Check if there is not any invalid models"
  [invalid-models]
  (nil? (seq invalid-models)))

(defn- pp-invalid-models
  "pprint invalid relations in models"
  [invalid-models]
  (clj-pp/print-table (map (fn [[_ error]] error) invalid-models)))

(defn assert-models-relations
  "Check if all relations in models are between valid participants"
  [relation-validator model-dictionary]
  (let [n 128
        roles-dic (dictionary/roles-dic model-dictionary)
        relations-dic (dictionary/relations-dic model-dictionary)
        <relations (chan n)
        >results (chan n (remove (fn [[valid _]] valid)))]
    (pipe
      (async/merge
        (map
          (fn [[{:keys [relation roles]} models]]
            (go
              [(<! (<valid? relation-validator roles))
               {:relation relation
                :models   models}]))
          relations-dic))
      >results)
    (let [invalid-models (<!! (async/into [] >results))]
      (assert
        (any-invalid-models? invalid-models)
        (format "Invalid relations." (pp-invalid-models invalid-models))))))

(defrecord ModelsValidator [relation-validator model-dictionary]
  component/Lifecycle
  (start [this]
    (log/trace "ModelsValidator started")
    (assert-models-relations relation-validator model-dictionary)
    this)
  (stop [this]
    (log/trace "ModelsValidator stopped")
    this))

(defn ->models-validator
  "Create component for model validator"
  []
  (map->ModelsValidator {}))
