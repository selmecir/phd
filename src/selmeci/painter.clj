(ns selmeci.painter
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [selmeci.canvas.core :as c]))

(defprotocol IDraw
  (draw! [painter model]
    "Draw models to canvas"))

(defrecord Painter [canvas]
  component/Lifecycle
  (start [this]
    (log/trace "Painter started")
    this)
  (stop [this]
    (log/trace "Painter stopped"))

  IDraw
  (draw! [this model]
    (c/add-model! canvas model)))

(defn ->painter
  "Return component which draws on canvas"
  []
  (map->Painter {}))
