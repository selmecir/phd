(ns selmeci.pattern-vocabulary.initialization.neo4j
  (:require [clojure.spec.alpha :as s]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [selmeci.pattern-vocabulary.core :as pv :refer [PATTERN_ROLE]]
            [clojure.string :as clj-str]
            [com.edocu.help.db.neo4j :as neo4j]
            [clojure.core.async :as async :refer [<!! go]]
            [selmeci.help.utils :as utils]
            [selmeci.canvas.core :refer [PARTICIPANT]]))

(defn- entities-roles->cypher
  "Define entity role as participant"
  [entity-role]
  (let [entity-role (utils/->PascalCaseString
                      entity-role)]
    {:statement (format
                  "CREATE (%s:%s {name: {%s}})"
                  entity-role
                  PATTERN_ROLE
                  entity-role)
     :params    {entity-role entity-role}}))

(s/fdef entities-roles->cypher
        :args (s/cat :entity-role ::pv/entity-role)
        :ret string?)

(defn- entities-hierarchies->cypher
  "Define hierarchies among entities"
  [[entity-role parent-role]]
  (let [entity-role (utils/->PascalCaseString
                      entity-role)
        parent-role (utils/->PascalCaseString
                      parent-role)]
    {:statement (format
                  "MATCH (%s:%s {name: {%s}}),(%s:%s {name: {%s}})
                   MERGE (%s)-[:isA]->(%s)"
                  entity-role
                  PATTERN_ROLE
                  entity-role
                  parent-role
                  PATTERN_ROLE
                  parent-role
                  entity-role
                  parent-role)
     :params    {entity-role entity-role
                 parent-role parent-role}}))

(s/fdef entities-hierarchies->cypher
        :args (s/cat :entity-hierarchy ::pv/entity-hierarchy)
        :ret string?)

(defn- relation->cypher
  "Convert relation to rdf statement"
  [[subject predicate object]]
  (let [subject (utils/->PascalCaseString
                  subject)
        predicate (utils/->camelCaseString
                    predicate)
        object (utils/->PascalCaseString
                 object)]
    {:statement (format
                  "MATCH (%s:%s {name: {subject_%s}}),(%s:%s {name: {object_%s}})
                   MERGE (%s)-[:%s]->(%s)"
                  subject
                  PATTERN_ROLE
                  subject
                  object
                  PATTERN_ROLE
                  object
                  subject
                  predicate
                  object)
     :params    {(str "subject_" subject) subject
                 (str "object_" object)   object}}))

(s/fdef relation->cypher
        :args (s/cat :relation ::pv/relation)
        :ret string?)

(defn- entity-roles->index-cypher
  "Return query for creating role index"
  [entity-role]
  {:statement (format
                "CREATE INDEX ON :%s(name)"
                (utils/->PascalCaseString entity-role))})

(defn- init-vocabulary-in-neo4j
  "Initialize pattern vocabulary in neo4j"
  [client {:keys [entities-roles
                  entities-hierarchies
                  relations-roles
                  relations]}]
  (let [entities-roles-as-indexes (future (mapv entity-roles->index-cypher entities-roles))
        entities-roles-as-pattern-participant (future (mapv entities-roles->cypher entities-roles))
        entities-hierarchies-as-cypher (future (mapv entities-hierarchies->cypher entities-hierarchies))
        relations-as-cypher (future (mapv relation->cypher relations))
        statements (concat
                     [{:statement (format "CREATE INDEX ON :%s(name)" PATTERN_ROLE)}
                      {:statement (format "CREATE INDEX ON :%s(name)" PARTICIPANT)}]
                     @entities-roles-as-indexes
                     @entities-roles-as-pattern-participant
                     @entities-hierarchies-as-cypher
                     @relations-as-cypher)]
    (<!! (neo4j/<transaction-run client statements))))

(defrecord PatternVocabulary [client vocabularies entities-roles entities-hierarchies relations-roles relations]
  component/Lifecycle
  (start [this]
    (log/trace "PatternVocabulary in Neo4J started")
    (try
      (let [vocabulary
            (reduce
              (fn [{:keys [entities-roles
                           entities-hierarchies
                           relations-roles
                           relations] :as tmp}
                   vocabulary]
                (assoc tmp
                  :entities-roles (into entities-roles (:entities-roles vocabulary))
                  :entities-hierarchies (into entities-hierarchies (:entities-hierarchies vocabulary))
                  :relations-roles (into relations-roles (:relations-roles vocabulary))
                  :relations (into relations (:relations vocabulary))))
              {:entities-roles       #{}
               :entities-hierarchies #{}
               :relations-roles      #{}
               :relations            #{}}
              vocabularies)]
        (init-vocabulary-in-neo4j client vocabulary)
        (merge this
               vocabulary))
      (catch Exception e
        (log/error e "PatternVocabulary")
        this)))
  (stop [this]
    (log/trace "PatternVocabulary in Neo4J stopped")
    this))

(defn ->pattern-vocabulary
  "Return component with pattern vocabulary"
  [patterns]
  (map->PatternVocabulary {:vocabularies (map :vocabulary patterns)}))
