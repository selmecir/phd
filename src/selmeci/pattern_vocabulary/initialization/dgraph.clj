(ns selmeci.pattern-vocabulary.initialization.dgraph
  (:require [clojure.spec.alpha :as s]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [selmeci.pattern-vocabulary.core :as pv]
            [clojure.string :as clj-str]
            [selmeci.help.dgraph :as dgraph]
            [clojure.core.async :as async :refer [<!!]]))

(defn- entities-roles->participant-rdf
  "Define entity role as participant"
  [entity-role]
  (format
    "_%s <isA> _:PatternParticipant .
     _%s <name> \"%s\" ."
    entity-role
    entity-role
    (name entity-role)))

(s/fdef entities-roles->participant-rdf
        :args (s/cat :entity-role ::pv/entity-role)
        :ret string?)

(defn- entities-hierarchies->participant-rdf
  "Define hierarchies among entities"
  [[entity-role parent-role]]
  (format
    "_%s <isA> _%s ."
    entity-role
    parent-role))

(s/fdef entities-hierarchies->participant-rdf
        :args (s/cat :entity-hierarchy ::pv/entity-hierarchy)
        :ret string?)

(defn- relation->rdf
  "Convert relation to rdf statement"
  [[subject predicate object]]
  (format
    "_%s <%s> _%s ."
    subject
    (name predicate)
    object))

(s/fdef relation->rdf
        :args (s/cat :relation ::pv/relation)
        :ret string?)

(defn- relations->dGraph-pattern-vocabulary-mutation
  "Return query with mutation for initialize all pattern roles"
  [{:keys [entities-roles
           entities-hierarchies
           relations-roles
           relations]}]
  (let [entities-roles-as-pattern-participant (future (map entities-roles->participant-rdf entities-roles))
        entities-hierarchies-as-rdf (future (map entities-hierarchies->participant-rdf entities-hierarchies))
        relations-as-rdf (future (map relation->rdf relations))]
    (format
      "mutation{
        schema {
          name: string @index(term, hash, trigram) .
          isA: uid @count @reverse .
        }
        set{
          _:PatternParticipant <name> \"PatternParticipant\" .
          %s
          %s
          %s
        }
      }"
      (clj-str/join "\n" @entities-roles-as-pattern-participant)
      (clj-str/join "\n" @entities-hierarchies-as-rdf)
      (clj-str/join "\n" @relations-as-rdf))))

(defn- init-vocabulary-in-dgraph
  "Initialize pattern vocabulary in dgraph"
  [dgraph vocabulary]
  (let [init (<!!
               (dgraph/<execute-query!
                 dgraph
                 (relations->dGraph-pattern-vocabulary-mutation vocabulary)))]
    (log/trace
      "init-vocabulary-in-dgraph:"
      init)
    init))

(defrecord PatternVocabulary [dgraph vocabularies entities-roles entities-hierarchies relations-roles relations]
  component/Lifecycle
  (start [this]
    (log/trace "PatternVocabulary started")
    (let [vocabulary
          (reduce
            (fn [{:keys [entities-roles
                         entities-hierarchies
                         relations-roles
                         relations] :as tmp}
                 vocabulary]
              (assoc tmp
                :entities-roles (into entities-roles (:entities-roles vocabulary))
                :entities-hierarchies (into entities-hierarchies (:entities-hierarchies vocabulary))
                :relations-roles (into relations-roles (:relations-roles vocabulary))
                :relations (into relations (:relations vocabulary))))
            {:entities-roles       #{}
             :entities-hierarchies #{}
             :relations-roles      #{}
             :relations            #{}}
            vocabularies)]
      (init-vocabulary-in-dgraph dgraph vocabulary)
      (merge this
             vocabulary)))
  (stop [this]
    (log/trace "PatternVocabulary stopped")
    this))

(defn ->pattern-vocabulary
  "Return component with pattern vocabulary"
  [patterns]
  (map->PatternVocabulary {:vocabularies (map :vocabulary patterns)}))
