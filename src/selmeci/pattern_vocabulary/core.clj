(ns selmeci.pattern-vocabulary.core
  (:require [clojure.spec.alpha :as s]
            [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]))

(def ^:const PATTERN_ROLE "PatternRole")

(s/def ::role (s/or
                :one keyword?
                :multiple (s/coll-of keyword? :min-count 1 :distinct true :kind set?)))
(s/def ::roles (s/coll-of ::role :min-count 1 :distinct true :kind set?))
(s/def ::entity-role ::role)
(s/def ::entities-roles ::roles)
(s/def ::relation-role ::role)
(s/def ::relations-roles ::roles)
(s/def ::relation (s/cat :subject ::entity-role
                         :predicate ::relation-role
                         :object ::entity-role))
(s/def ::relations (s/coll-of ::relation :min-count 1 :distinct true :kind set?))
(s/def ::entity-hierarchy (s/cat :role ::entity-role
                                 :parent ::entity-role))
(s/def ::entities-hierarchies (s/coll-of ::entity-hierarchy :distinct true :kind set?))

(defn valid-relation?
  "Check if subject, object and predicat from relation belong to roles"
  [entities-roles relations-roles [subject predicate object]]
  (some? (and (entities-roles subject)
              (entities-roles object)
              (relations-roles predicate))))

(s/fdef valid-relation?
        :args (s/cat :entitis-roles ::entities-roles
                     :relations-roles ::relations-roles
                     :relation ::relation)
        :ret boolean?)

(defn- valid-relations?
  "Check if all relations are among defined roles"
  [pattern-vocabulary]
  (every? (fn [{:keys [subject predicate object]}]
            (valid-relation?
              (::entities-roles pattern-vocabulary)
              (::relations-roles pattern-vocabulary)
              [subject predicate object]))
          (::relations pattern-vocabulary)))

(s/def ::patter-vocabulary
  (s/and (s/keys :req [::entities-roles
                       ::entities-hierarchies
                       ::relations-roles
                       ::relations])
         valid-relations?))

(defrecord PatternVocabulary
  [pattern-name
   entities-roles
   entities-hierarchies
   relations-roles
   relations]
  component/Lifecycle
  (start [this]
    (assert
      (s/valid?
        ::patter-vocabulary
        {::entities-roles       entities-roles
         ::entities-hierarchies entities-hierarchies
         ::relations-roles      relations-roles
         ::relations            relations})
      (format "Invalid vocabulary for patter: %s" pattern-name))
    (log/trace "Start Pattern Vocabulary for: " pattern-name)
    this)
  (stop [this]
    (log/trace "Stop Pattern Vocabulary for: " pattern-name)
    this))
