(ns selmeci.run
  (:gen-class)
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [selmeci.help.dgraph :as dgraph]
            [com.edocu.help.db.neo4j :as neo4j]
            [selmeci.pattern-vocabulary.initialization.neo4j :as vocabulary-initialization]
            [selmeci.model.validation :as validation]
            [selmeci.model.validation.neo4j :as relation-validation]
            [selmeci.canvas.neo4j :as canvas]
            [selmeci.painter :as painter]
            [selmeci.model.dictionary :as model-dic]
            [selmeci.pattern-variant.detection :as pv-detection]

            [selmeci.pattern.canonical-protocol :as canonical-protocol]
            [selmeci.pattern.canonical-schema :as canonical-schema]
            [selmeci.pattern.entity-abstraction :as entity-abstraction]
            [selmeci.pattern.logic-centralization :as logic-centralization]
            [selmeci.pattern.process-abstraction :as process-abstraction]
            [selmeci.pattern.redundant-implementation :as redundant-implementation]
            [selmeci.pattern.schema-centralization :as schema-centralization]
            [selmeci.pattern.service-data-replication :as service-data-replication]
            [selmeci.pattern.service-layers :as service-layer]
            [selmeci.pattern.trusted-subsystem :as trusted-subsystem]
            [selmeci.pattern.utility-abstraction :as utility-abstraction]

            [selmeci.uses-cases.canonical-protocol :as canonical-protocol-use-case]
            [selmeci.uses-cases.canonical-schema :as canonical-schema-use-case]
            [selmeci.uses-cases.entity-abstraction :as entity-abstraction-use-case]
            [selmeci.uses-cases.logic-centralization :as logic-centralization-use-case]
            [selmeci.uses-cases.process-abstraction :as process-abstraction-use-case]
            [selmeci.uses-cases.redundant-implementation :as redundant-implementation-use-case]
            [selmeci.uses-cases.schema-centralization :as schema-centralization-use-case]
            [selmeci.uses-cases.service-data-replication :as service-data-replication-use-case]
            [selmeci.uses-cases.service-layers :as service-layers-use-case]
            [selmeci.uses-cases.trusted-subsystem :as trusted-subsystem-use-case]
            [selmeci.uses-cases.utility-abstraction :as utility-abstraction-use-case]))

(defn- in-pattern
  [section patterns]
  (flatten (map section patterns)))

(def ^{:private true :doc "Return all problems in patterns"} problems-in-patterns (partial in-pattern :problems))
(def ^{:private true :doc "Return all solutions in patterns"} solutions-in-patterns (partial in-pattern :solutions))

(defn ->system
  "Return system"
  [env]
  (let [canonical-protocol (canonical-protocol/->pattern)
        canonical-schema (canonical-schema/->pattern)
        entity-abstraction (entity-abstraction/->pattern)
        logic-centralization (logic-centralization/->pattern)
        process-abstraction (process-abstraction/->pattern)
        redundant-implementation (redundant-implementation/->pattern)
        schema-centralization (schema-centralization/->pattern)
        service-data-replication (service-data-replication/->pattern)
        service-layer (service-layer/->pattern)
        trusted-subsystem (trusted-subsystem/->pattern)
        utility-abstraction (utility-abstraction/->pattern)
        patterns [canonical-protocol
                  canonical-schema
                  entity-abstraction
                  logic-centralization
                  process-abstraction
                  redundant-implementation
                  schema-centralization
                  service-data-replication
                  service-layer
                  trusted-subsystem
                  utility-abstraction]]
    (component/system-map
      :neo4j (neo4j/->neo4j-client)

      :relation-validator (component/using
                            (relation-validation/->relation-validator)
                            {:client :neo4j})

      :canvas (component/using
                (canvas/->canvas)
                {:client :neo4j})

      :painter (component/using
                 (painter/->painter)
                 [:canvas])

      :pattern-vocabulary (component/using
                            (vocabulary-initialization/->pattern-vocabulary
                              patterns)
                            {:client :neo4j})

      :model-dictionary (component/using
                          (model-dic/->model-dictionary)
                          [:canvas])

      :model-validation (component/using
                          (validation/->models-validator)
                          {:relation-validator                :relation-validator
                           :model-dictionary                  :model-dictionary
                           ;additional dependencies
                           :pattern-vocabulary                :pattern-vocabulary
                           :canonical-protocol-use-case       :canonical-protocol-use-case
                           :canonical-schema-use-case         :canonical-schema-use-case
                           :entity-abstraction-use-case       :entity-abstraction-use-case
                           :logic-centralization-use-case     :logic-centralization-use-case
                           :process-abstraction-use-case      :process-abstraction-use-case
                           :redundant-implementation-use-case :redundant-implementation-use-case
                           :schema-centralization-use-case    :schema-centralization-use-case
                           :service-data-replication-use-case :service-data-replication-use-case
                           :service-layers-use-case           :service-layers-use-case
                           :trusted-subsystem-use-case        :trusted-subsystem-use-case
                           :utility-abstraction-use-case      :utility-abstraction-use-case})

      :problems-detector (component/using
                           (pv-detection/->pattern-variant-detector
                             :problems
                             (problems-in-patterns patterns))
                           {:client           :neo4j
                            :model-validation :model-validation})

      :solutions-detector (component/using
                            (pv-detection/->pattern-variant-detector
                              :solutions
                              (solutions-in-patterns patterns))
                            {:client           :neo4j
                             :model-validation :model-validation})

      ;; USE CASES

      :canonical-protocol-use-case (component/using (canonical-protocol-use-case/->use-case) [:painter :pattern-vocabulary])

      :canonical-schema-use-case (component/using (canonical-schema-use-case/->use-case) [:painter :pattern-vocabulary])

      :entity-abstraction-use-case (component/using (entity-abstraction-use-case/->use-case) [:painter :pattern-vocabulary])

      :logic-centralization-use-case (component/using (logic-centralization-use-case/->use-case) [:painter :pattern-vocabulary])

      :process-abstraction-use-case (component/using (process-abstraction-use-case/->use-case) [:painter :pattern-vocabulary])

      :redundant-implementation-use-case (component/using (redundant-implementation-use-case/->use-case) [:painter :pattern-vocabulary])

      :schema-centralization-use-case (component/using (schema-centralization-use-case/->use-case) [:painter :pattern-vocabulary])

      :service-data-replication-use-case (component/using (service-data-replication-use-case/->use-case) [:painter :pattern-vocabulary])

      :service-layers-use-case (component/using (service-layers-use-case/->use-case) [:painter :pattern-vocabulary])

      :trusted-subsystem-use-case (component/using (trusted-subsystem-use-case/->use-case) [:painter :pattern-vocabulary])

      :utility-abstraction-use-case (component/using (utility-abstraction-use-case/->use-case) [:painter :pattern-vocabulary])

      ;;;;;;;;;;;;;;;;;

      )))

(def ^:private system (atom nil))

(defn -main [& args]
  (log/set-level! :trace)
  (log/merge-config!
    {:ns-blacklist ["selmeci.help.dgraph"]})
  (reset! system (component/start (->system :dev)))
  @system)

(comment
  (let [model-validation (:model-validation @system)
        canvas (:canvas @system)
        use-case (:use-case @syste)]
    (component/stop canvas)
    (component/stop model-validation)
    (component/start canvas)
    (component/start use-case)
    (component/start model-validation)))