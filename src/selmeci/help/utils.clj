(ns selmeci.help.utils
  (:require [clojure.spec.alpha :as s]
            [clojure.core.async :as a]
            [clojure.core.async.impl.protocols :as ap]
            [taoensso.timbre :as log]
            [camel-snake-kebab.core :as camel]
            [clojure.string :as clj-str]
            [clojure.core.memoize :as memo]
            [com.edocu.help.db.neo4j :as neo4j]))

(defmacro chan-of [spec & chan-args]
  `(let [ch# (a/chan ~@chan-args)]
     (reify
       ap/ReadPort
       (take! [_ fn1-handler#]
         (ap/take! ch# fn1-handler#))
       ap/WritePort
       (put! [_ val# fn1-handler#]
         (if (s/valid? ~spec val#)
           (ap/put! ch# val# fn1-handler#)
           (do
             (ap/close! ch#)
             (put-in-mdc {:validation-err (s/explain-data ~spec val#)
                          :val            val#})
             (log/error (str "Spec failed to validate: " (s/explain-str ~spec val#))))))
       ap/Channel
       (close! [_]
         (ap/close! ch#))
       (closed? [_]
         (ap/closed? ch#)))))

(s/def ::read-channel #(satisfies? clojure.core.async.impl.protocols/ReadPort %))
(s/def ::write-channel #(satisfies? clojure.core.async.impl.protocols/WritePort %))

(def ->PascalCaseString
  (memo/fifo
    camel/->PascalCaseString
    :fifo/threshold 1024))

(def ->camelCaseString
  (memo/fifo
    camel/->camelCaseString
    :fifo/threshold 1024))