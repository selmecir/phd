(ns selmeci.help.dgraph
  (:require [clojure.spec.alpha :as s]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [environ.core :as e]
            [clojure.core.async :as async :refer [close! >! <! chan put! go]]
            [org.httpkit.client :as http]
            [cheshire.core :as json]
            [selmeci.help.utils :as utils]))

(def ^:const n 128)

(defprotocol dGraphManager
  (<execute-query! [this query] "Execute query in dgraph and return channel with response"))

(s/def ::graph-manager #(satisfies? dGraphManager %))
(s/def ::query string?)

(s/fdef <execute-query!
        :args (s/cat :this ::graph-manager
                     :query ::query)
        :ret ::utils/read-channel)

(defrecord dGraphRequest [uri query >result])

(s/def ::uri string?)
(s/def ::>result ::utils/write-channel)
(s/def ::dGraphRequest (s/keys :req-un [::uri ::query ::>result]))

(def num-request (atom 0))

(defn- dgraph-request
  "Make request on dgraph"
  [{:keys [uri query >result]} >callback]
  (let [options {:body query}
        uuid (swap! num-request + 1)
        finish (fn []
                 (log/trace "dgraph-request. finished:" uuid)
                 (close! >result)
                 (put! >callback :ok (fn [_] (close! >callback))))]
    (log/trace "dgraph-request. started:" uuid)
    (try
      (http/post
        uri
        options
        (fn [{:keys [status body error]}]
          (if (or error
                  (not= 200 status))
            (do
              (log/error error "dgraph-request. status:" status)
              (finish))
            (try
              (let [response (json/parse-string body true)]
                (go
                  (when-let [data (:data response)]
                    (>! >result data))
                  (finish)))
              (catch Exception e
                (log/error e "dgraph-request. body:" body)
                (finish))))))
      (catch Exception e
        (log/error e "dgraph-request")
        (finish)))))

(s/fdef dgraph-request
        :args (s/cat :request ::dGraphRequest
                     :>callback ::utils/write-channel))

(defn- ->jobs-handler []
  (let [>jobs (chan n)]
    (async/pipeline-async
      n
      (chan (async/sliding-buffer 4))
      dgraph-request
      >jobs)
    >jobs))

(s/fdef ->jobs-handler
        :ret ::utils/write-channel)

(defrecord dGraph [uri >jobs]
  component/Lifecycle
  (start [this]
    (log/trace "gGraph client started")
    (assoc this
      :uri (e/env :dgraph-uri)
      :>jobs (->jobs-handler)))
  (stop [this]
    (log/trace "gGraph client stopped")
    (dissoc this :uri))

  dGraphManager
  (<execute-query! [this query]
    (let [>result (chan)]
      (put! >jobs (->dGraphRequest
                    uri
                    query
                    >result))
      >result)))

(defn ->dgraph []
  (map->dGraph {}))
